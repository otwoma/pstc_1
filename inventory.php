<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 4/28/2017
 * Time: 3:49 PM
 */

include '_header_store.php';


$sno=sani($_POST['s_no']);
$name=sani($_POST['item_name']);
$unit=sani($_POST['unit']);
$category=sani($_POST['category']);
$quantity=sani($_POST['quantity']);
$quantity=intval($quantity);
$threshold=intval($_POST['threshold']);
$remark=sani($_POST['remark']);


if(($name!='' && $quantity!='')){

    m("INSERT INTO consumables SET s_no = '".$sno."', item_name = '".$name."', unit = '".$unit."', category ='".$category."' , threshold='".$threshold."', quantity='".$quantity."', remark = '".$remark."'");

    header("Location:store/inventory.php");
}else {
    header("Location:store/inventory.php?error=101");
}

$item_taker=cleaN($_POST['taker_t']);
$item_taken_by=cleaN($_POST['taker_id']);
$givetran_note = cleaN($_POST['gtran_note']);
$item_fo_gid=cleaN($_POST['give_item_lead']);
$give_n_items=cleaN($_POST['given_itms']);
if(isset($item_taker) && $item_taken_by !='' && isset($item_fo_gid)){
    $giid=store_get_item_id_give($item_fo_gid,$give_n_items);
    $mid_tranid = rand_letters(3).$stock_batch.time();
    foreach($giid as $item_d){
        m("INSERT INTO store_moves SET mid='".$mid_tranid."',item_id = '".$item_d."'");
        m("UPDATE consumables SET status = 0 WHERE s_no = '".$item_d."'");

    }
    m("INSERT INTO store_motion SET holder = '".$item_taken_by."',move_id = '".$mid_tranid."',holder_group  ='".$item_taker."',move_note='".$givetran_note."'");
}
?>

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#consumableTable').dataTable();
            $('#receivingTable').dataTable();
        } );
    </script>


    <div id="main-container">

    <ul class="tab-bar grey-tab">
            <li class="protab">
                <a href="#consumables" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-cart-plus fa-2x"></i>
                    </span>
                    Consumables In
                </a>
            </li>

            <li class="protab">
                <a href="#consumables_out" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-life-ring fa-2x"></i>
                    </span>
                    Consumables Out
                </a>
            </li>
        </ul>

        <div class="padding-md">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                <div class="padding-sm font-16">
                            Store Items
                            <a href="#newStoreitem" role="button" data-toggle="modal" class="btn btn-info btn-small" style="margin-left: 900px"><span class="fa fa-plus"></span> Add Item</a>
                        </div>
                    <div class="tab-content">
                        <div class="tab-pane fade in active " id="consumables">
                            <div class="panel panel-default table-responsive">
                                <div class="seperator"></div><div class="seperator"></div>
                                <table class="table table-striped" id="consumableTable">
                                    <thead>
                                    <tr>
                                        <th width="" align="left"><span class=""></span>S/NO</th>
                                        <th width="" align="left"><span class=""></span>Category</th>
                                        <th width="" align="left"><span class=""></span>Item Name</th>
                                        <th width="" align="left"><span class=""></span>Initial Quantity</th>
                                        <th width="" align="left"><span class=""></span>Stock Qty</th>
                                        <th width="" align="left"><span class=""></span>Percentage Out</th>
                                        <th width="" align="center"></th>
                                        <th width="" align="center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $ask=m("SELECT id,s_no,item_name,quantity, category, threshold,unit FROM consumables WHERE status =1");
                                        while($data=mray($ask)){
                                            $id = $data['id'];
                                            $label = $data['item_name'];
                                            $sno = $data['s_no'];
                                            $cat = $data['category'];
                                            $quantity = $data['quantity'];
                                            $unit = $data['unit'];
                                            $threshold_qr = $data['threshold'];

                                            $remaining=count_remaining_consumable($id);
                                            $dif=$quantity-$remaining;

                                            $tage_figure=(($dif/$quantity)*100);
                                            $tage=number_format($tage_figure,2).'%';

                                            $threshold=consumable_threshold_level($id);
                                        ?>
                                        <tr>
                                            <td align="left"><?php echo $sno;?></td>
                                            <td><?php echo category_label($cat);?></td>
                                            <td align="left"><?php echo item_label($label)?></td>
                                            <td title="<?php echo 'Threshold Mark: '.$threshold_qr;?>"><?php echo $quantity;?></td>
                                            <td><?php echo $remaining;?></td>
                                            <td title="<?php echo number_format((100-$tage_figure),2).'%'.' Stock Remaining:'?>"><?php echo $tage;?></td>

                                            <?php
                                            if($remaining==0){
                                                //RESTOCK
                                            ?>

                                            <td><a class="btn btn-danger" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-exclamation"></i> Out of Stock</a></td>
                                                <?php }elseif($remaining<=$threshold){?>
                                            <td title="Threshold Quantity: <?php echo $threshold;?>"><a  class="btn btn-warning " data-name="<?php echo $label;?>"  data-id="<?php echo $id;?>"><i class="fa fa-caret-right"></i> Issue out</a></td>
                                                <?php }else{?>
                                            <td><a href="#storeGive" role="button" data-toggle="modal" class="btn btn-info item_give" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-caret-right"></i> Issue out</a></td>
                                                <?php }?>
                                            <td><a href="purchase_request" class="btn btn-primary" data-name="<?php echo $label;?>" data-id="<?php echo $id;?>"><i class="fa fa-refresh"></i> Restock</a></td>

                                            </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="consumables_out">
                            <div class="panel panel-default table-responsive">
					<div class="padding-sm font-16">
					<span class="fa fa-external-link"></span> Items Given Out

					</div>
                    <div class="seperator"></div><div class="seperator"></div>
				</div>


                <table class="table table-striped" id="receivingTable">
                    <thead>
                    <tr>
                        <th width="" align="left"><span class=""></span>Name</th>
                        <th width="" align="left"><span class=""></span>Group Category</th>
                        <th width="" align="left"><span class=""></span>ID</th>
                        <th width="" align="left"><span class=""></span>Item Label</th>
                        <th width="" align="left"><span class=""></span>Pieces</th>
                        <th width="" align="left"><span class=""></span>Time Taken</th>
                        <th width="" align="left"><span class=""></span></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    //$list = "SELECT batch,id,label,type,cnd,date_added FROM store_items ORDER BY id ASC";
                    $list = "SELECT move_id,move_note,holder,move_time,holder_group FROM store_motion WHERE move_back  = ''";
                    $list_query = mysql_query($list);
                    $coun=0;
                    while($list_result = mysql_fetch_array($list_query)){
                        $themd=$list_result['move_id'];
                        $move_note = $list_result['move_note'];
                        $holder_id = $list_result['holder'];
                        $time_taken = $list_result['move_time'];
                        $item_holder = item_holder_details($holder_id,$list_result['holder_group']);
                        $item_lid= fetch_item_id($themd);
                        $item_label = get_item_label($item_lid);
                        $nitems_b = count_items_taken($themd);
                        $trake_gr=$item_holder[1];
                        $aker_nae= $item_holder[0];;
                        ?>
                        <tr>
                            <td align="left"><?php echo $aker_nae;?></td>
                            <td align="left"><?php echo $trake_gr;?></td>
                            <td align="left"><?php echo $holder_id?></td>
                            <td align="left"><?php echo $item_label;?></td>
                            <td align="left"><?php echo $nitems_b;?></td>
                            <td align="left"><?php echo $time_taken;?></td>
                            <td align="center"><a class='item_receive' href='#storeReceive' data-ti="<?php echo $time_taken;?>" data-taker="<?php echo $aker_nae.' ('.$trake_gr.')';?>" data-ni="<?php echo $nitems_b.' piece'.plu($nitems_b);?>" data-iteml="<?php echo $item_label;?>" data-moveid="<?php echo $themd;?>" role='button' data-toggle='modal'><button class='butn butn-blue'><span class='fa fa-level-down'></span> Receive</button></a></td>

                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="newStoreitem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Stock</h4>
            </div>
            <div class="modal-body">
                <form id="tab1-form" role="form" class="container-fluid" action="" method="post">
                                <div class="row">
                                    <div class="form-group">
                                        <label>S/NO</label></br>
                                        <input placeholder="S/No." type="" name="s_no" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Category</label></br>
                                        <select class="form-control" style="margin-left:0px;" name="category">
                                            <?php categories() ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Item Name</label></br>
                                        <select name="item_name" id="" class="form-control">
                                            <?php item_name() ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                       <label>Quantity</label></br>
                                       <input placeholder="Quantity" type="text" name="quantity" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                       <label>Threshold Quantity</label></br>
                                       <input placeholder="Threshold Quantity (Optional)" type="number" name="threshold" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                       <label>Unit of Measure</label></br>
                                       <select name="unit" id="" class="form-control">
                                            <?php echo store_unit_options()?>
                                       </select>
                                    </div>

                                    <div class="form-group">
                                       <label>Remark</label></br>
                                       <textarea placeholder="Remark" type="text" name="remark" class="form-control" required></textarea>
                                    </div>
                                </div>

            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button  type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="storeGive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Giving Out</h4>
                    <span class="fa fa-chevron-right"> <span id="item_labelG" class="font-14"></span></span>

                </div>
                <div class="modal-body">
                    <form id="gve_outfom" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadG" name="give_item_lead"/>
                        <input hidden="" id="pmax" name="pimax"/>
                        </span>

                        <div class="form-group">
                            <label>Pieces</label>
                            <input id="pitem_c" onchange="handleChange(this);" style="width: 55px;" value="1" type="text" name="given_itms" class="form-control" required>
                            <script>
                                $('#pitem_c').on("keyup",function (mtc) {
                                    var max = $("#pmax").val();
                                    var mvax = $(this).val();
                                    if(mvax > max){
                                        alert('Currently the maximum number of pieces that can be given is '+max);
                                        $(this).val(max);
                                    }
                                });
                            </script>
                        </div>

                        <div class="form-group">
                            <label>Giving</label></br>
                            <select id="gvtaker_sel" class="form-control" style="width: 300px;" name="taker_t">
                                <?php store_give_out_options()?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label id="taker_i">Recruit ID.</label> <label id="taker_"></label>
                            <input style="font-size: 16px;" id="taken_id" type="text" name="taker_id" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label>Note: </label>
                            <textarea class="form-control input-sm" name="gtran_note" style="height: 55px;"></textarea>
                        </div>

                        <span id="taker_nameclue"></span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <div class="modal fade" id="storeReceive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-nice-dark2 white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <span id="item_labelR" class="font-14 bold"></span></span>
                    </br> </br>
             <span id="exp_items" class="font-14 bold margin-sm"></span>
                </div>

                <div class="modal-body">
                    <form id="rcv_inform" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadR" name="give_item_lead"/></span>

                        <div class="form-group bold">
                           Given to: <span class="font-14 bold" id="recv_from"></span>
                        </div>

                            <div align="right" class="ital">Given <span id="gvn_itGo"></span></div>

                                <input hidden class="hidden" type="text" name="motion_tg" id="motion_tg" required/>


                        <div class="form-group">
                            <span class="text-wrap bold" id="move_note_xq"></span>
                        </div>


                </div>
                <div class="modal-footer">
                    <button class="btn butn-white btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn butn-navy btn-sm white">Receive</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <script> $(".inventory_mu").addClass('active');

        $("body").on('click','.item_give',function(){
            var ilabel= $(this).data("name");
            var glead= $(this).data("lead");
            var mx= $(this).data("maxi");
            $("#item_labelG").html(ilabel);
            $("#item_leadG").val(glead);
            $("#pmax").val(mx);
        });
        function handleChange(input) {
            var qax=  $("#pmax").val();
            if (input.value < 0) input.value = 0;
            if (input.value > qax) input.value = qax;
        }

        $("body").on('click','.item_receive',function(){
            var ilabel= $(this).data("name" +"");
            var glead= $(this).data("lead");
            $("#item_labelR").html(ilabel);
            $("#item_leadR").val(glead);

            $.ajax({
                url: '../_ajax.php',
                type: 'POST',
                data: $('#rcv_inform').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;
                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });

        });


        $("body").on('change','#gvtaker_sel',function(){
            $("#taker_").html('');
            $("#taken_id").val('');
            var tak = $(this).val();
            var label = $("#taker_i");
            if(tak==1){label.html("Recruit ID.")}
            if(tak==2){label.html("Instructor ID.")}
        });

        $('#taken_id').on("keyup",function (tnane) {
            $("#taker_").html('');
            $.ajax({
                url: '../_ajax.php',
                type: 'POST',
                data: $('#gve_outfom').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;

                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });
        });

        $("body").on('click','.item_receive',function(){
        var motion_id= $(this).data("moveid");
        var motion_item= $(this).data("iteml");
        var i_n= $(this).data("ni");
        var mvtime= $(this).data("ti");
        $("#item_labelR").html(motion_item);
        $("#exp_items").html(i_n);
        $("#motion_tg").val(motion_id);

        $("#recv_from").html($(this).data("taker"));

        $.post('../_ajax.php', 'timeago='+mvtime,
            function (time_ago) {
                console.log(time_ago);
                $('#gvn_itGo').html(time_ago)
            });
        $.post('../_ajax.php', 'store_item_note='+motion_id,
            function (mnote) {
                $('#move_note_xq').html(mnote)
            });

    });
    </script>



<?php include'../_footer.php';?>