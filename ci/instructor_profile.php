<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/3/2017
 * Time: 10:10 AM
 */

include '_header_ci.php';

$instructor_id = sani($_REQUEST['td']);

if(intval($instructor_id)==0){
    header('dashboard.php');

}else{

    $instructor = m("SELECT * FROM instructors WHERE service_number = $instructor_id");
    $instructor_data = msoc($instructor);

    $service_number = $instructor_data['service_number'];
    $full_name = $instructor_data['first_name'].' '.$instructor_data['other_name'].' '.$instructor_data['surname'];
}

?>

<style>
    .bg-pr-green{background-color:#9D352A;}
    .pr-green,.green2{color:#1E6C42;}
</style>

<div id="main-container">
    <div class="padding-md">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="row">
                    <div class="col-xs-9 col-sm-12 col-md-9">
                        <h4 class="headline bold font-16 bg-primary padding-sm br-2 white bg-pr-green">
                            <?php echo $full_name;?>
                            <span class="line" style="background-color:#fff;"></span>
                        </h4>

                        <img src="<?php //$images=$avad.student_avatar($td);
                        echo $avad . 'user.png';
                        ?>" alt="Instructor Photo" class="profile_photo">

                        <div class="seperator"></div>
                    </div>
                </div>

                <div class="row bold font-16 padding-md"> <?php echo 'Service Number: <span class="green2">' . $service_number; ?></span>

                    <br>
                    <br>

                    <?php

                    $instructor_assigned = assign_label($instructor_data['assigned']);
                    $status = status_label($instructor_data['status']);
                    $designation = designation_name($instructor_data['designation_id']);
                    ?>

                    <p class="font-14"> <?php echo 'Assigned: <span class="green2  ">' . $instructor_assigned; ?></span></p>
                    <p class="font-14"> <?php echo 'Status: <span class="green2  ">' . $status; ?></span></p>
                    <br>
                </div>
            </div>

            <div class="col-md-9 col-sm-9">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="overview">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-default fadeInDown animation-delay2">
                                    <div class="padding-xs bg-pr-green white">
                                        <span class="padding-xs font-14"><span class="fa fa-address-card"></span> Contact Details</span>
                                    </div>
                                    <div class="panel-body font-14 border-1">
                                        <ul class="list-group">
                                            <li class="list-group-item bg-light">

                                                <p><strong class="font-normal">Phone Number: </strong><span
                                                        class="font-normal green2"><?php echo $phone = $instructor_data['phone'] ?></span>
                                                </p>


                                                <p><strong class="font-normal">Email: </strong><span
                                                        class="font-normal green2"><?php echo $email = $instructor_data['email_address'] ?></span>
                                                </p>

                                                <h5 class="headline bold font-15 butn-white padding-sm br-2">
                                                    Contact <?php echo $full_name?>
                                                    <span class="line"></span>
                                                </h5>

                                                <a data-phone="<?php echo $phone; ?>"
                                                   data-name="<?php echo $full_name; ?>" data-rel="Instructor"
                                                   class="sms_home" href="#sendSMS" role="button"
                                                   data-toggle="modal">
                                                    <button class="btn butn-navy"><span
                                                            class="fa fa-envelope"></span> Send Text Message
                                                    </button>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="panel panel-default fadeInDown animation-delay2">
                                    <div class="padding-xs bg-pr-green white">
                                        <span class="padding-xs font-14"><span class="fa fa-id-badge"></span> Rank Details</span>
                                    </div>
                                    <div class="panel-body font-14 border-1">
                                        <ul class="list-group">
                                            <li class="list-group-item bg-light">

                                                <p><strong class="font-normal">Designation: </strong><span
                                                        class="font-normal green2"><?php echo $designation;  ?></span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script> $('.profile_mu').addClass('active')</script>

    <div class="modal fade" id="sendSMS">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>New SMS to <span id="relat"></span></h4>
                </div>
                <div class="modal-body">
                    <form action="_handle_sms" method="POST">
                        <input value="direct" name="action" type="hidden" class="hidden">

                        <div class="form-group">
                            <label>Send to :</label>
                            <input id="phone_x" class="form-control" name="phone" type="text">
                        </div>

                        <div class="form-group">
                            <label>Message :</label>
                            <textarea required class="form-control input-sm sms_con" name="the_sms"
                                      style="height: 120px;"></textarea>
                            <span class="pull-right margin-sm bold font-14" id="sms_counter"></span>
                        </div>
                        <span>Sending SMS to <span class="bold" id="name_rec"></span> </span>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="butn butn-blue btn-sm"><i class="fa fa-send"></i> Send</button>
                    <button class="btn  btn-sm" data-dismiss="modal" aria-hidden="true"> Close</button>
                </div>

                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="composeDIMS">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>New Message</h4>
                </div>
                <div class="modal-body">
                    <form action="_handle_ims" method="POST">

                        <div class="form-group hidden">
                            <input hidden="hidden" value="G" required class="form-control" name="message_target"/>
                            <input hidden="hidden" value="<?php echo $parent_email; ?>" required class="form-control"
                                   name="gamail"/>
                            <input hidden="hidden" value="<?php echo $phon1; ?>" required class="form-control"
                                   name="gauser"/>
                            <input hidden="hidden" value="<?php echo $ppid; ?>" required class="form-control"
                                   name="tdp"/>
                        </div>

                        <div class="form-group">
                            <label>To: </label>
                            <input disabled value="<?php echo $guname . ' (' . $sname_posss . ')'; ?>" type="text"
                                   name="message_title" class="form-control" required>

                        </div>

                        <div class="form-group">
                            <label>Message Title :</label>
                            <input placeholder="eg. PTA Meeting" type="text" name="message_title" class="form-control"
                                   required/>

                        </div>

                        <div class="form-group">
                            <label>Message :</label>
                            <textarea required class="form-control input-sm" name="the_message"
                                      style="height: 120px;"></textarea>
                        </div>


                        <span>Message will be forwarded to <?php echo $parent_email; ?> </span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <script>


        $(document).on("click", ".sms_home", function () {
            var number = $(this).data('phone');
            var name = $(this).data('name');
            var relation = $(this).data('rel');

            $("#phone_x").val(number);
            $("#name_rec").html(name);
            $("#relat").html(relation);


        });

    </script>

<?php include '../_footer.php'?>