<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit98377505c4882a873c38419d26c356b8
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'DocxMerge\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'DocxMerge\\' => 
        array (
            0 => __DIR__ . '/..' . '/krustnic/docx-merge/src',
        ),
    );

    public static $classMap = array (
        'DocxMerge\\DocxMerge' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge.php',
        'DocxMerge\\DocxMerge\\Docx' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge/Docx.php',
        'DocxMerge\\DocxMerge\\Prettify' => __DIR__ . '/..' . '/krustnic/docx-merge/src/DocxMerge/Prettify.php',
        'DocxMerge\\libraries\\TbsZip' => __DIR__ . '/..' . '/krustnic/docx-merge/src/libraries/TbsZip.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit98377505c4882a873c38419d26c356b8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit98377505c4882a873c38419d26c356b8::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit98377505c4882a873c38419d26c356b8::$classMap;

        }, null, ClassLoader::class);
    }
}
