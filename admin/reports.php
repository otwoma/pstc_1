<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 6/5/2017
 * Time: 6:22 PM
 */

include'_header_admin.php';
?>


    <div id="main-container">
        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Qualifications Master Reports</h3>
            </div>
        </div>

        <div class="padding-md">
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
                        <span class="pull-left bold">
                           <i class="fa fa-bed"></i> Recruit - Barrack Report
                        </span>

                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="../reports/barracks.php" method="post">
                                    <div class="form-group margin-sm">
                                        <label>Select Barrack:</label>

                                        <select class="form-control" name="barrack_name" id="barrack_name">


                                            <option value="all">All Barracks</option>

                                            <?php $brk = m("SELECT * FROM barracks");


                                            while($brks = mray($brk)){
                                                ?>
                                                <option value="<?php echo $brks['barrack_id'];?>"> <?php echo $brks['barrack_name'];?></option>
                                            <?php }?>


                                        </select>

                                    </div>

                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-archive"></i> Recruit - Tribe Report
                            </span>
                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="../reports/recruit_tribe_report.php" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Select Tribe Report:</label>

                                        <select class="form-control" name="tribe" id="tribe">


                                            <option value="all">Select All Tribes</option>

                                            <?php $companu = m("SELECT * FROM tribes");


                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['id'];?>"> <?php echo $companies['tribe_name'];?></option>
                                            <?php }?>


                                        </select>

                                        <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                                class="fa fa-send"></span> Get Report
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-map-marker"></i> Recruits - Location Report
                            </span>
                        </div>
                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>County:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">


                                            <option value="all">Select All Counties</option>

                                            <?php $companu = m("SELECT * FROM counties");


                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['id'];?>"> <?php echo $companies['county_name'];?></option>


                                            <?php }?>


                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div style="background-color: #356934; font-size:13.5px;" class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-user"></i> Recruits Report
                            </span>
                        </div>

                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Company Filter:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">
                                            <option value="all">Select All Recruits</option>

                                            <?php $companu = m("SELECT * FROM companies WHERE status = 1");

                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['company_id'];?>"> <?php echo $companies['company_name'];?></option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group margin-sm">
                                        <label>Platoon Filter:</label>
                                        <select class="form-control" name="clearance_type" id="clearance_type">
                                            <option value="all">Select All Recruits</option>

                                            <?php $companu = m("SELECT * FROM platoon");

                                            while($companies = mray($companu)){
                                                ?>
                                                <option value="<?php echo $companies['platoon_id'];?>"> <?php echo $companies['platoon_name'];?></option>

                                            <?php }?>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary pull-right margin-sm"><span
                                            class="fa fa-send"></span> Get Report
                                    </button>

                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>



                </div>

                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading3 clearfix"
                             style="background-color: #356934; font-size:13.5px;">
                        <span class="pull-left bold">
                                <i class="fa fa-cart-arrow-down"></i> Issued Items
                        </span>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <h5>Issued Items <br>Report</h5>

                                <form action="#" method="post">
                                    <select id="driver_select" class="form-control m-b smart_select" name="driver">
                                        <option value="">All Items</option>
                                        <?php ; ?>
                                    </select>

                                    <br>

                                    <p>Select Date Range
                                        <small>
                                            <small>(Date Format: d-m-Y)</small>
                                        </small>
                                    </p>
                                    <div class="input-daterange input-group" id="date">
                                        <input required placeholder="From (d-m-Y)" type="text"
                                               class="input-sm form-control" name="date_from"/>
                                        <span class="input-group-addon">to</span>
                                        <input readonly placeholder="To (d-m-Y)" type="text"
                                               class="input-sm form-control" name="date_end"/>
                                    </div>

                                    <br>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> Get
                                        Report
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>$(".reports_mu").addClass('active')</script>

    <script src="../assets/js/select2.min.js"></script>
    <script src="../assets/js/bootstrap-timepicker.min.js"></script>
    <script src="../assets/js/bootstrap-datepicker.min.js"></script>
    <script>
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $.fn.datepicker.defaults.autoclose = "true";
        $('.form-group.date._date').datepicker({});
        $('#date').datepicker({});
        $('.input-daterange').datepicker({});
    </script>

<?php include '../_footer.php' ?>