<?php
include "_header.reg.php";
?>

    <div id="main-container">

        <div class="padding-md">

            <div class="page-title">
                <h3 class="no-margin">Certificate Generator</h3>
                <br>
            </div>


            <div class="row">


                <p style="font-size:16px;"> <i class="fa fa-info-circle"></i> Certificates will be available for download from <strong>14<sup>th</sup> October 2017</strong></p>


            <div class="col-lg-5" style=" margin-left:2%; padding:12px;background-color: #DDF1DC;color:#77777F; font-size:13px; border-radius:5px; border:2px solid #D1E6D0">

                This will generate and download certificates for all the <?php echo count_recruits();?> recruits.  <br> (The certificates will be in ready to print PDF format)

            </div>

            </div>


            <div class="row">


                <div class="col-lg-5" style="margin-left:2%;">

            </br>

        <a href="certificate_master.php">
            <button class="btn btn-primary"> <i class="fa fa-download"></i> Generate and Download Certificates  </button>
        </a>

                </div
            </div>

        </div>


    </div>



    <script> $(".certificate_generator").addClass('active');
        setInterval(function () {
            $.post('../_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>

       </br>
            </br>
            </br>
            </br>



<?php include '../_footer.php'; ?>