<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 6/15/2017
 * Time: 5:01 PM
 */

include'_header.reg.php';

?>

<div id="main-container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="#formModal" role="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-edit"></i> Compose Request</a>
            </div>

            <div class="panel-body no-padding">
                <div class="tab-left">
                    <ul class="tab-bar padding-sm">
                        <li class="active"><a href="#staff_mes" data-toggle="tab"><i class="fa fa-linux"></i> Store Messages</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="staff_mes">
                            <?php echo ims_display(10002,'registry')?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="formModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>New Message</h4>
            </div>
            <div class="modal-body">
                <form action="../_handle_ims" method="POST">

                    <div class="form-group hidden">
                        <label>Send to :</label>
                        <select required class="form-control" style="width: 300px;" name="message_target">
                            <?php echo message_target();?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Item Name :</label>
                        <select required class="form-control" style="width: 300px;" name="message_title">
                            <?php echo store_items_()?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Message :</label>
                        <textarea required class="form-control input-sm" name="the_message" style="height: 120px;"></textarea>
                    </div>



            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-danger btn-sm">Submit</button>
            </div>
        </div>
    </div>
    </form>
</div>

<div class="modal fade" id="openIMS">
    <div class="modal-dialog">
        <div class="modal-content" id="ims_read">
        </div>
    </div>

</div>

<script> $(".store_mu").addClass('active');
    $(".xoims").on('click',function () {
        var dx=$(this).data("wim");
        $.post('../_ajax', 'read_ims='+dx,
            function (uil) {
                $("#ims_read").html(uil);
            });
    });
</script>
<?php include'../_footer.php';?>
