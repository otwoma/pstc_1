<?php include '_header.reg.php';
?>
<div id="main-container">

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
			<div class="padding-md">
				<div class="panel panel-default table-responsive">
					<div class="padding-sm font-16">
					<i class="fa fa-group"></i>	Registry Staff
					</div>

                    <div class="seperator"></div><div class="seperator"></div>
					<table class="table table-striped" id="responsiveTable">
						<thead>
							<tr>
                                <th width="" align="left"><span class=""></span>Name</th>
                                <th width="" align="left"><span class=""></span>PF. Number</th>
                                <th width="250" align="left"><span class=""></span>Permission</th>
                                <th><span class=""></span></th>
							</tr>
						</thead>
						<tbody>
                        <?php
                        $list = "SELECT * FROM accounts WHERE status = 1 AND account_type = 'registry-staff'";
                        $list_query= $d->q($list);
                        while($list_result = msoc($list_query)){

                            $full_name= $list_result['name'];
                            $pf_number = $list_result['username'];

                            $user_account_id = $list_result['id'];
                            $permission_label = read_registry_staff_permissions($user_account_id);
                            ?>
                            <tr title="<?php echo $made_title;?>">
                                <td><?php echo $full_name;?></span></td>
                                <td align="left"><?php echo $pf_number;?></td>
                                <td align="left" style="font-size: 14px;"><?php echo $permission_label?></td>
                                <td align="left">
                                    <a data-name="<?php echo $full_name;?>" data-pfn="<?php echo $pf_number;?>" href="#formModal" role="button" data-toggle="modal" class="btn btn-sm btn-primary edit_reg_staff"><span class="fa fa-edit"></span> Edit Permissions</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>







<div class="modal fade" id="formModal">
    <div class="modal-dialog">
        <div class="modal-content" id="targetContentHolder">


        </div>
    </div>

</div>






<script> $(".registry_staff").addClass('active');

    $(document).on("click", ".edit_reg_staff", function () {
        var name = $(this).data('name');
        var pfn = $(this).data('pfn');
        var target = $("#targetContentHolder");
        var account_id =  $(this).data('account_id');

        $.post('_ajax.php', {
                name: name,pfn:pfn,intent:'assign_registry_permissions',account_id:account_id
        },
            function (response) {
                target.html(response);
            });

    });





</script>

<?php include '../_footer.php'; ?>
