<?php
/*Code by :: NEO*/

include ('../_functions.php');
include ('../_variables.php');
include ('_auth-registry.php');

error_reporting(1);
require_once '../packages/Classes/PHPWord.php';
//require_once '../packages/Classes/PHPWord/Autoloader.php';


$PHPWord = new PHPWord();

/*$rendererName = PHPWord_Settings::PDF_RENDERER_MPDF;
$rendererLibrary = 'mPDF5.7';
$rendererLibraryPath = '../packages/Classes/pdf/';

if (!PHPWord_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
    die(
        'NOTICE: Something ain`t right, kindly contact system developers' .
        '<br />' .
        'ERRORCODE: PDF_LIB404'
    );
}*/

$extension = 'docx';

$ask=mysql_query("SELECT * FROM recruits WHERE status = 1");

while($smart=mysql_fetch_assoc($ask)) {

    $recruit_name = trim($smart['surname']).' '.trim($smart['first_name']).' '.trim($smart['other_name']);
    $recruit_name = trim(strtoupper(strtolower($recruit_name)));
    $sto_number = trailing_zeros($smart['sto_number'])  ;
    $platoon_id = $smart['platoon_id'];
    $document = $PHPWord->loadTemplate('xj.docx');
    $cert_number = $smart['recruit_id']+52411;

    $document->setValue('Value3', $recruit_name);
    $document->setValue('Value1', '');
    $document->setValue('Value2', '');
    $document->setValue('Value4', $cert_number);


    $document->setValue('weekday', date('dS-M-Y'));


    $file_name_x = $recruit_name .'-'.$sto_number;
    $file_x = 'certs/'.$file_name_x.'.'.$extension;

    $cert_location[] = $file_x;
    $cert_title[] = $file_name_x.'.'.$extension;

    $document->save($file_x);



}

$zip_uniq_id = strtoupper(uniqid());
$zip_folder_name = date('F Y').' Certificates_'.$zip_uniq_id.'.zip';

$zip = new ZipArchive();

if ($zip->open("$zip_folder_name", ZipArchive::CREATE) !== TRUE) {
    die("Could not open archive");
}

$irp=0;

foreach ($cert_location as $cert_path){
    $zip->addFile($cert_path, $cert_title[$irp]) or die ("ERROR: Could not add file: $cert_path");
    $irp++;
}

$zip->close();

header("Content-type: application/force-download");
header("Content-Transfer-Encoding: Binary");
header("Content-length: ".filesize($zip_folder_name));
header("Content-disposition: attachment; filename=".basename($zip_folder_name)."");
readfile("$zip_folder_name");