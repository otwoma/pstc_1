<?php
include "_functions.php";
include "_variables.php";

if($_POST['list_item']!=''){
    $task = cleaN($_POST['list_item']);
    $user = $tid;
    $task_pid=task_add($user,$task);

    echo "<li id='$task_pid' class='list-group-item'>
                <label title='Mark as done' class='label-checkbox inline'>
                    <input data-did='$task_pid' type='checkbox' class='task-finish'>
                    <span class='custom-checkbox'></span>
                </label>
                          $task
                <span title='delete task' class='pull-right'>
                    <a data-id='$task_pid' href='#' class='task-del'><i class='fa fa-trash-o fa-lg text-danger'></i></a>
                </span>
           </li>
    ";
}

if($_POST['delete_task']!=''){
    $task_id = cleaN($_POST['delete_task']);
    $user=$tid;
    task_delete($user,$task_id);

}

if($_POST['done_task']!=''){
    $task_id = cleaN($_POST['done_task']);
    $user=$tid;
    task_done($user,$task_id);

}

if($_POST['undo_task']!=''){
    $task_id = cleaN($_POST['undo_task']);
    $user=$tid;
    task_undo($user,$task_id);

}

if(isset($_POST['password']) && isset($_POST['new_password'])){

    $old_password = cleaN($_POST['password']);
    $new_passoword = cleaN($_POST['new_password']);
    $new_passoword_s = cleaN($_POST['new_password_s']);
    $old_password = trim($old_password);
    $new_passoword = trim($new_passoword);
    $new_passoword_s = trim($new_passoword_s);

    if($new_passoword==$new_passoword_s){
        if(strlen($new_passoword_s)<4){
            echo 0;
            die();
        }else{
            $user_hdpass= pass_encode($old_password);
            $hd_pass= md(m("SELECT password FROM accounts WHERE username = '".$tid."' AND account_type = '".$act."'"));
            if($user_hdpass==$hd_pass){
                $new_pass = pass_encode($new_passoword_s);

                if($new_pass==$hd_pass){echo 44; die();}

                m("UPDATE accounts SET password = '".$new_pass."' WHERE username = '".$tid."' AND account_type = '".$act."'");
                echo 77;
            }else{
                echo 15;
            }
        }
    }else{echo 13;}

}

if(isset($_POST['purchase_data_row']) && $_POST['extra']==77){
    $next=$_POST['next'];
    $int_ra=rand(1,3);
    $rand_=rand_string($int_ra);
    $c_time=uniqid().$rand_;
    ?>

    <tr>
        <td title="Code No."><input class="form-control" placeholder="Code No." value="<?php echo $next; ?>" type="text" name="item_code[]"></td>
        <td title="Item Description"><input class="form-control" placeholder="Item Description" type="text" name="item_description[]"></td>
        <td title="Unit of Issue"><input class="form-control" placeholder="Unit of Issue" type="text" name="item_issue_unit[]"></td>
        <td title="Required Quantity"><input class="form-control" placeholder="Quantity " type="text" name="item_qty_received[]"></td>
        <td title="Issued Quantity"><input class="form-control" placeholder="Unit Cost" type="text" name="item_cost[]"></td>
        <td title="Value"><input class="form-control" placeholder="Total Amount" type="text" name="item_value[]"></td>
        <td title="Value"><input class="form-control" placeholder="S13 Balance" type="text" name="s13_balance[]"></td>
        <td title="Value"><input class="form-control" placeholder="Last Purchase Date" type="text" name="last_purchase_date[]"></td>
        <td title="Remarks/Purpose"><input class="form-control" placeholder="Remarks" type="text" name="item_remarks[]"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>

    <?php
}

if(isset($_POST['timeago'])){
    $time_stp = $_POST['timeago'];
    $str_to_time90 = strtotime($time_stp);
    echo time_ago($str_to_time90);
}

if($_POST['read_ims']!=''){
    $read_i=cleaN($_POST['read_ims']);
    $ims_a=ims_sent_message($read_i);
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?php echo $ims_a[0];?></h4>
    </div>
    <div class="modal-body">
        <div class="font-14 border-1 padding-sm text-wrap ims_cm">
            <?php echo $ims_a[1];?>
        </div>

    </div>
    <div class="modal-body bold">
        Sent: <span> <?php echo date("dS-M-Y H:i",$ims_a[2]);?></span>
        <span class="inline-block pull-right"><?php echo 'Requested by: '.$ims_a[3]?></span>
    </div>

    <div class="modal-footer">
        <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
    <?php
}

if($_POST['ims_mark']!=''){
    $ims_idm= cleaN($_POST['ims_mark']);
    $g_forstdid= cleaN($_POST['xs_gims']);
    ims_mark_read($ims_idm,$g_forstdid,$act);
}

if(isset($_POST['taker_id'])){

     $takernb=cleaN($_POST['taker_id']);
     $taker_id = $_POST['taker_t'];
    if($taker_id==1 && strlen($takernb>=$min_recruit_id_len)){

        echo recruit_name($takernb);
    }elseif($taker_id==2  && strlen($takernb>=$min_instructor_id_len)){
        echo get_instructor_name($takernb);
    }
}

if(isset($_POST['store_item_note'])){
    $item= $_POST['store_item_note'];
    $note=store_motion_note($item);
    if($note==''){echo 'Note: N/A';}else{echo 'Note: '.$note;}
}