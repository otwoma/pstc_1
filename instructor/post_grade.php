<?php include '_header.instructor.php';


$instructor_service_number = $_SESSION['_user_id'];

$company_id = $_REQUEST['company_id'];
$class_id = $_REQUEST['class_id'];
$unit_id = $_REQUEST['unit_id'];

$instructor_id = instructor_id($instructor_service_number);

?>

<div id="main-container" class="col-lg-8">
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
    <div class="padding-md">

        <div class="padding-sm font-16 bg-grey" align="left">
            <?php
            if(isset($class_id) && $unit_id!=''){
                $vhcont='';
                $unit_title=ucwords(strtolower(unit_name($unit_id)));

                $company_name = company_name($company_id);
                $post_grade_title = $unit_title.' - '.class_label($class_id,' Class ').' ('.$company_name.')';

                $bold_part = '';
                echo 'Grades for'.'<strong> '. $bold_part.' <small>'.$post_grade_title.'</small></strong>';
            }else{ $vhcont='hidden';$rew_subject='';$rew_classlab='';echo "<div class='animate1 fadeIn font-normal'>  <i class='fa fa-list'></i>  <strong> Your classes </strong><br> Following is a list of your classes</div>";}?>
        </div>


        <div class="panel panel-default table-responsive">


            <div class="seperator"></div><div class="seperator"></div>
            <form class="<?php echo $vhcont;?>" name="grader" id="grade_form" method="post"  action="handle_grade.php">


                <table class="table table-striped" id="responsiveTable">
                    <thead>
                    <tr>
                        <th align="left"><span class=""></span>Student Name</th>
                        <th align="left"><span class=""></span>Sto No.</th>
                        <th width="12%" align="left"><span class=""></span> Cat  30%</th>
                        <th width="12%" align="left"><span class=""></span>Exams 70%</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $list_query = mysql_query("SELECT recruit_id FROM recruit_class_allocation WHERE class_id = $class_id AND company_id = $company_id AND unit_id = $unit_id AND status = 1");
                    $grade_inputs=0;
                    while($list_result = mysql_fetch_array($list_query)){
                        $recruit_id = $list_result['recruit_id'];

                        $recruit_name = recruit_name($recruit_id);

                        $recruit_sto = recruit_sto($recruit_id);

                        $grade_check = check_grade_submission($class_id,$unit_id,$company_id,$recruit_id);

                        $recruit_platoon = recruit_platoon_name($recruit_id);

                        $single_name = explode(' ',$recruit_name)[0];

                        if($grade_check==0){
                            $grade_inputs++
                            ?>
                            <tr title="Platoon: <?php echo $recruit_platoon;?>">
                                <td align="left"><?php echo $recruit_name;?></td>
                                <td align="left"><?php echo $recruit_sto;?></td>
                                <td title="<?php echo $single_name."'s - ".'Cat Mark';?>" align="left">
                                        <input type="text"   name="cat"  class="form-control font-15 score" />
                                </td>
                                <td title="<?php echo $single_name."'s - ".$rew_subject.'Exam Mark';?>" align="left">
                                    <input type="text"  name="exam"  class="form-control font-15 score" />
                                </td>
                            </tr>
                            <?php

                        }

                        else{

                            $cat_mark = fetch_grade($class_id,$unit_id,$company_id,$recruit_id,'cat_mark');
                            $exam_mark = fetch_grade($class_id,$unit_id,$company_id,$recruit_id,'exam_mark');

                            ?>

                            <tr title="Platoon: <?php echo $recruit_platoon;?>">
                                <td align="left"><?php echo $recruit_name;?></td>
                                <td align="left"><?php echo $recruit_sto;?></td>
                                <td title="<?php echo $single_name."'s - ".'Cat Mark';?>" align="left">
                                    <input type="text" value="<?php echo $cat_mark;?>" name="cat"  class="form-control font-15 score" />
                                </td>
                                <td title="<?php echo $single_name."'s - ".$rew_subject.'Exam Mark';?>" align="left">
                                    <input type="text" value="<?php echo $exam_mark;?>" name="exam"  class="form-control font-15 score" />
                                </td>
                            </tr>

                            <?php
                        }
                    }
                    ?></tbody>
                </table>
                <input hidden="hidden" value="<?php echo $class_tag;?>" name="gr_classid"/>
                <input hidden="hidden" value="<?php echo $subj;?>" name="gr_subjectid"/>
                <input hidden="hidden" value="<?php echo $tgg_term;?>" name="gr_term"/>
                <input hidden="hidden" value="<?php echo $tgg_year;?>" name="gr_year"/>
                <input hidden="hidden" value="<?php echo $tid;?>" name="stf_idf"/>
                <div align="center">
                    <?php if($grade_inputs!=0){?><button id="grade_class" type="submit" style="margin-top: 38px; margin-bottom: 10px" class="btn btn-success"><span class="fa fa-check"></span> Submit Grade<?php echo plu  ($grade_inputs);?></button><?php }else{}?>
                </div>

            </form>
        </div>
    </div>
</div>


<script> $(".grade_mu").addClass('active');


    $(document).on("change", ".score", function () {
        var el = $(this);
        var target_id = el.data("id");
        var grade = el.val();

        var remark_el = $("#remark_"+target_id);
        remark_el.prop('required',false);

        if(grade==1){
            remark_el.hide();
            remark_el.val('');
        }

        if(grade==0){

            remark_el.show();
            remark_el.val('');

        }
        if(grade==3){

            remark_el.show();
            remark_el.val('');
            remark_el.prop('required',true);
        }




    });

    $(function onchange_subject() {
        $('#myclasses_drop_stf').change(function() {
            this.form.submit();
        });
    });

    function sp_subjects(){
        var class_id = $("#class_id").val();
        var placer= $('#myclasses_drop_stf');
        var staff_id = '<?php echo $_SESSION['_user_id'];?>';


        $.post('_ajax.php',{class_id:class_id,staff_id_subject_options:staff_id},function(subject_options_cl){
            console.log(subject_options_cl);
            placer.html(subject_options_cl);

        });

    }

    $(function onchange_class() {
        $('#class_id').change(function() {
            sp_subjects();
        });
    });

    sp_subjects();


</script>
<?php include'../_footer.php';?>
