<?php
include "_header_qm.php";

?>
    <script type="text/javascript" src="../assets/js/chart-js/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="../assets/js/chart-js/utils.js"></script>

    <div id="main-container">


        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard <small>Control Panel</small></h3>
            </div>
        </div>

        <div class="padding-md">

            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-green">

                        <h1 class="m-top-none" id="student_count">
                            <?php echo count_recruits(); ?>
                        </h1>
                        <h5 class="bold">Recruit Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-graduation-cap fa-3x"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-blue">

                        <h1 class="m-top-none" id="staff_count">
                            <?php echo count_instructors(); ?>
                        </h1>
                        <h5 class="bold">Instructor Population</h5>

                        <div class="stat-icon">
                            <i class="fa fa-briefcase fa-3x"></i>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="panel-stat3 bg-cyan">

                        <h1 class="m-top-none" id="student_count">
                            <?php echo count_platoons(); ?>
                        </h1>
                        <h5 class="bold">Platoons</h5>

                        <div class="stat-icon">
                            <i class="fa fa-users fa-3x"></i>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    <div class="padding-md">

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading3 clearfix">
                            <span class="pull-left bold">
                               <i class="fa fa-graduation-cap"></i> Access Recruit Profile
                            </span>
                        </div>

                        <div class="panel-body no-padding collapse in">
                            <ul class="list-group task-list no-margin collapse in">
                                <form action="../_actions" method="get">
                                    <div class="form-group margin-sm">
                                        <label>Recruit Store Number:</label>
                                        <input type="text" name="quick_ac_std_pro" class="form-control"
                                               placeholder="Sto. Number" required></br>
                                        <button type="submit" class="butn butn-navy pull-right margin-sm"><span
                                                class="fa fa-angle-double-right"></span> See Profile
                                        </button>
                                    </div>
                                </form>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix">
                            <span class="pull-left bold">
                                <span class="text-success m-left-xs"><i class="fa fa-check"></i></span> To Do List
                            </span>
                            <ul class="tool-bar">
                                <li><a href="#do_listModal" role="button" data-toggle="modal"
                                       class="bg-success btn-small"><i class="fa fa-plus"></i> New Task</a>
                                </li>
                                <li><a href="#toDoListWidget" data-toggle="collapse"><i class="fa fa-arrow-up"></i><i
                                            class="fa fa-arrow-down"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body no-padding collapse in" id="toDoListWidget">
                            <ul class="list-group task-list no-margin collapse in">

                                <span id="new_tasks_holder"></span>

                                <?php echo tasks_display($tid); ?>
                            </ul>
                        </div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row center col-lg-offset-1" align="center" >
            <div class="col-sm-5 col-md-5" style="margin-right:10px;background-color: #fff; border:3px solid #356934; border-radius:7px;">
                <div class="panel-stat">
                    <canvas id="genderChart"></canvas>
                </div>
            </div>
            <div class="col-sm-5 col-md-5" style="margin-left:10px;background-color: #fff; border:3px solid #356934; border-radius:7px;">
                <div class="panel-stat">
                    <canvas id="platoonChart"></canvas>
                </div>
            </div>
        </div>

    </div>




    <div class="modal fade" id="do_listModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="close_modal" type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4>New item</h4>
                </div>

                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Task :</label>
                            <textarea spellcheck="false" required class="form-control input-sm sms_con" id="todo_item"
                                      name="list_item" style="height: 70px;"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button id="todo_add" type="button" class="btn butn-green btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <script> $(".dashboard_mu").addClass('active');
        setInterval(function () {
            $.post('../_ajax', 'online_users=0',
                function (live_users) {
                    $("#holder_users_online").html(live_users);
                });
        }, 1000);
    </script>

<?php include '../_footer.php'; ?>


<script>
    var data = [12, 19, 3, 5, 2, 3];
    var labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
    var bgColor = ["#179483","#A7CA74","#A3CBF0","#697480","#5FE28B","#F2CA84","#9d5f51","#EE422B","#1485BA","#1AC9B9","#674265","#FE6599","#26327F","#FC6E51","#9367C8","#000000","#FFAA31","#B61F24"];
    var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
    var ctx = document.getElementById("genderChart");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        palette: "Soft Pastel",
        data: {
            labels: labels,
            datasets: [
                {
                    data: data,
                    backgroundColor: bgColor
                }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: { padding: 15,fontSize:14.5}
            },

            title: {
                display: true,fontSize:15.5,
                text: 'Gender Distribution'
            },
            tooltips: {
                mode: 'single',
                titleFontSize:14.2,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ': ' + datasetLabel + '';
                    }
                }
            }
        }


    });

</script>

<script>
    var data = [12, 19, 3, 5, 2, 3];
    var labels = ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"];
    var bgColor = ["#179483","#A7CA74","#A3CBF0","#697480","#5FE28B","#F2CA84","#9d5f51","#EE422B","#1485BA","#1AC9B9","#674265","#FE6599","#26327F","#FC6E51","#9367C8","#000000","#FFAA31","#B61F24"];
    var default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
    var ctx = document.getElementById("platoonChart");
    var myChart = new Chart(ctx, {
        type: 'pie',
        palette: "Soft Pastel",
        data: {
            labels: labels,
            datasets: [
                {
                    data: data,
                    backgroundColor: bgColor
                }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: { padding: 15,fontSize:14.5}
            },

            title: {
                display: true,fontSize:15.5,
                text: 'Platoon Gender Distribution'
            },
            tooltips: {
                mode: 'single',
                titleFontSize:14.2,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.labels[tooltipItem.index];
                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        return label + ': ' + datasetLabel + '';
                    }
                }
            }
        }


    });

</script>