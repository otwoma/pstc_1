
$(function	()	{
    var skin_use = $.cookie('skin_color');

    $("#"+skin_use).addClass('current-theme');

	if(jQuery.type($.cookie('skin_color')) != 'undefined')	{
	
		$('aside').removeClass('skin-1');
		$('aside').removeClass('skin-2');
		$('aside').removeClass('skin-3');
		$('aside').removeClass('skin-4');
		$('aside').removeClass('skin-5');
		$('aside').removeClass('skin-6');
        $('aside').removeClass('skin-7');
        $('aside').removeClass('skin-8');
        $('aside').removeClass('skin-9');
        $('aside').removeClass('skin-10');
		$('#top-nav').removeClass('skin-1');
		$('#top-nav').removeClass('skin-2');
		$('#top-nav').removeClass('skin-3');
		$('#top-nav').removeClass('skin-4');
		$('#top-nav').removeClass('skin-5');
		$('#top-nav').removeClass('skin-6');
        $('#top-nav').removeClass('skin-7');
        $('#top-nav').removeClass('skin-8');
        $('#top-nav').removeClass('skin-9');
        $('#top-nav').removeClass('skin-10');
		
		$('aside').addClass($.cookie('skin_color'));
		$('#top-nav').addClass($.cookie('skin_color'));
	}
	
	$('.theme-color').click(function()	{
        var skinlabel = $(this).attr('id');
		$.cookie('skin_color', skinlabel);

        $.post('_ajax.php',{theme_change:skinlabel},function(theme_change){
        });
        $(".current-theme").removeClass("current-theme");
        $("#"+skinlabel).addClass('current-theme');

        $('aside').removeClass('skin-1');
		$('aside').removeClass('skin-2');
		$('aside').removeClass('skin-3');
		$('aside').removeClass('skin-4');
		$('aside').removeClass('skin-5');
		$('aside').removeClass('skin-6');
        $('aside').removeClass('skin-7');
        $('aside').removeClass('skin-8');
        $('aside').removeClass('skin-9');
        $('aside').removeClass('skin-10');
		$('#top-nav').removeClass('skin-1');
		$('#top-nav').removeClass('skin-2');
		$('#top-nav').removeClass('skin-3');
		$('#top-nav').removeClass('skin-4');
		$('#top-nav').removeClass('skin-5');
		$('#top-nav').removeClass('skin-6');
        $('#top-nav').removeClass('skin-7');
        $('#top-nav').removeClass('skin-8');
        $('#top-nav').removeClass('skin-9');
        $('#top-nav').removeClass('skin-10');
		
		$('aside').addClass(skinlabel);
		$('#top-nav').addClass(skinlabel);

	});

	paceOptions = {
		startOnPageLoad: true,
		ajax: false, // disabled
		document: false, // disabled
		eventLag: false, // disabled
		elements: false
	};
	
	$('.login-link').click(function(e) {
		e.preventDefault();
		href = $(this).attr('href');
		
		$('.login-wrapper').addClass('fadeOutUp');

		setTimeout(function() {
			window.location = href;
		}, 900);
			
		return false;	
	});
	
	$('#logoutConfirm').popup({
		pagecontainer: '.container',
		 transition: 'all 0.3s'
	});
	
	$("#scroll-to-top").click(function()	{
		$("html, body").animate({ scrollTop: 0 }, 600);
		 return false;
	});
	
	$('.scrollable-sidebar').slimScroll({
		height: '100%',
		size: '0px'
	});
	
	$('aside li').hover(
       function(){ $(this).addClass('open') },
       function(){ $(this).removeClass('open') }
	)
	
	$('.openable > a').click(function()	{
		if(!$('#wrapper').hasClass('sidebar-mini'))	{
			if( $(this).parent().children('.submenu').is(':hidden') ) {
				$(this).parent().siblings().removeClass('open').children('.submenu').slideUp();
				$(this).parent().addClass('open').children('.submenu').slideDown();
			}
			else	{
				$(this).parent().removeClass('open').children('.submenu').slideUp();
			}
		}
		
		return false;
	});
		
	$('#sidebarToggle').click(function()	{
		$('#wrapper').toggleClass('sidebar-display');
		$('.main-menu').find('.openable').removeClass('open');
		$('.main-menu').find('.submenu').removeAttr('style');
	});
	
	$('#sizeToggle').click(function()	{
	
		$('#wrapper').off("resize");
	
		$('#wrapper').toggleClass('sidebar-mini');
		$('.main-menu').find('.openable').removeClass('open');
		$('.main-menu').find('.submenu').removeAttr('style');
	});
	
	if(!$('#wrapper').hasClass('sidebar-mini'))	{ 
		if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
			$('#wrapper').addClass('sidebar-mini');
		}
		else if (Modernizr.mq('(min-width: 869px)'))	{
			if(!$('#wrapper').hasClass('sidebar-mini'))	{
			}
		}
	}

	$('#menuToggle').click(function()	{
		$('#wrapper').toggleClass('sidebar-hide');
		$('.main-menu').find('.openable').removeClass('open');
		$('.main-menu').find('.submenu').removeAttr('style');
	});
	
	$(window).resize(function() {
		if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 868px)')) {
			$('#wrapper').addClass('sidebar-mini').addClass('window-resize');
			$('.main-menu').find('.openable').removeClass('open');
			$('.main-menu').find('.submenu').removeAttr('style');
		}
		else if (Modernizr.mq('(min-width: 869px)'))	{
			if($('#wrapper').hasClass('window-resize'))	{
				$('#wrapper').removeClass('sidebar-mini window-resize');
				$('.main-menu').find('.openable').removeClass('open');
				$('.main-menu').find('.submenu').removeAttr('style');
			}
		}
		else	{
			$('#wrapper').removeClass('sidebar-mini window-resize');
			$('.main-menu').find('.openable').removeClass('open');
			$('.main-menu').find('.submenu').removeAttr('style');
		}
	});
	
	$('#fixedSidebar').click(function()	{
		if($(this).prop('checked'))	{
			$('aside').addClass('fixed');
		}	
		else	{
			$('aside').removeClass('fixed');
		}
	});
	
	$('#inboxMenuToggle').click(function()	{
		$('#inboxMenu').toggleClass('menu-display');
	});
	
	$('.collapse-toggle').click(function()	{
	
		$(this).parent().toggleClass('active');
	
		var parentElm = $(this).parent().parent().parent().parent();
		
		var targetElm = parentElm.find('.panel-body');
		
		targetElm.toggleClass('collapse');
	});
	
	var login_count = $('#system_logins').text();
	
	$({numberValue: 0}).animate({numberValue: login_count}, {
		duration: 2500,
		easing: 'linear',
		step: function() { 
			$('#system_logins').text(Math.ceil(this.numberValue));
		}
	});
			
	var count_learning_files = $('#visible_files').text();
	
	$({numberValue: 0}).animate({numberValue: count_learning_files}, {
		duration: 2500,
		easing: 'linear',
		step: function() { 
			$('#visible_files').text(Math.ceil(this.numberValue));
		}
	});
	
	$('.refresh-widget').click(function() {
		var _overlayDiv = $(this).parent().parent().parent().parent().find('.loading-overlay');
		_overlayDiv.addClass('active');
		
		setTimeout(function() {
			_overlayDiv.removeClass('active');
		}, 2000);
		
		return false;
	});
		
	$('#chk-all').click(function()	{
		if($(this).is(':checked'))	{
			$('.inbox-panel').find('.chk-item').each(function()	{
				$(this).prop('checked', true);
				$(this).parent().parent().addClass('selected');
			});
		}
		else	{
			$('.inbox-panel').find('.chk-item').each(function()	{
				$(this).prop('checked' , false);
				$(this).parent().parent().removeClass('selected');
			});
		}
	});
	
	$('.chk-item').click(function()	{
		if($(this).is(':checked'))	{
			$(this).parent().parent().addClass('selected');		
		}
		else	{
			$(this).parent().parent().removeClass('selected');
		}
	});
	
	$('.chk-row').click(function()	{
		if($(this).is(':checked'))	{
			$(this).parent().parent().parent().addClass('selected');		
		}
		else	{
			$(this).parent().parent().parent().removeClass('selected');
		}
	});
	
	$('.image-wrapper').bind('touchstart', function(e) {
		$('.image-wrapper').removeClass('active');
		$(this).addClass('active');
    });
	
	$('.hover-dropdown').hover(
       function(){ $(this).addClass('open') },
       function(){ $(this).removeClass('open') }
	)


	$('.remove-file').click(function()	{
		$(this).parent().find('span').attr('data-title','No file...');
		$(this).parent().find('label').attr('data-title','Select file');
		$(this).parent().find('label').removeClass('selected');

		return false;
	});	


	

        $('body').on('click','.task-finish',function(){
        var tas_id=$(this).data('did');

		if($(this).is(':checked'))	{
			$(this).parent().parent().addClass('selected');

            $.post('_ajax.php','done_task='+tas_id,
                function(task_finish){
                });
		}
		else	{
			$(this).parent().parent().removeClass('selected');
            $.post('_ajax.php','undo_task='+tas_id,
                function(task_undone){
                });
		}
	});

    $('#todo_add').click(function()	{
        var value_hp=$("#todo_item");
      var item = value_hp.val();

        $.post('_ajax.php','todo_task='+item,
            function (tasked){

            $("#close_modal").trigger('click');

            $("#new_tasks_holder").append(tasked);

                value_hp.val('');

            });

    });




    $('body').on('click','.task-del',function(){
		var activeList = $(this).parent().parent();

		activeList.addClass('removed');
				var task_id=$(this).data('id');

			$.post('_ajax.php','delete_task='+task_id,
                function(task_deleted){
                    setTimeout(function() {
                        activeList.remove();
                    }, 1000);

                });

		return false;
	});
	
    $("[data-toggle=popover]").popover();
	
    $("[data-toggle=tooltip]").tooltip();
	
});

$(window).load(function() {

	Pace.stop();
	
	$('#overlay').fadeOut(500);
	
	$('body').removeAttr('class');
	
	$('#wrapper').removeClass('preload');
		
	if(!$('#wrapper').hasClass('sidebar-mini'))	{
		$('aside').find('.active.openable').children('.submenu').slideDown();
	}
});

$(window).scroll(function(){
		
	 var position = $(window).scrollTop();
	
	 if(position >= 200)	{
		$('#scroll-to-top').attr('style','bottom:8px;');	
	 }
	 else	{
		$('#scroll-to-top').removeAttr('style');
	 }
});

$(function	()	{

    //Flot Chart
    //Website traffic chart

    plot = $.plot($('#placeholder'), [init], options);

    $("<div id='tooltip'></div>").css({
        position: "absolute",
        display: "none",
        border: "1px solid #222",
        padding: "4px",
        color: "#fff",
        "border-radius": "4px",
        "background-color": "rgb(0,0,0)",
        opacity: 0.90
    }).appendTo("body");


    $("#placeholder").bind("plotclick", function (event, pos, item) {
        if (item) {
            $("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
            plot.highlight(item.series, item.datapoint);
        }
    });

    var animate = function () {
        $('#placeholder').animate( {tabIndex: 0}, {
            duration: 3000,
            step: function ( now, fx ) {

                var r = $.map( init.data, function ( o ) {
                    return [[ o[0], o[1] * fx.pos ]];
                });

                plot.setData( [{ data: r }] );
                plot.draw();
            }
        });
    }

    animate();

    //Morris Chart
    var donutChart = Morris.Donut({
        element: 'donutChart',
        data: [
            {label: "Download Sales", value: 1236},
            {label: "In-Store Sales", value: 3091},
            {label: "Mail-Order Sales", value: 2781}
        ],
        colors: ['#f3ce85','#65CEA7' ,'#FC8675']
    });

    var lineChart = Morris.Line({
        element: 'lineChart',
        data: [
            { y: '2006', a: 30,  b: 20 },
            { y: '2007', a: 45,  b: 35 },
            { y: '2008', a: 60,  b: 60 },
            { y: '2009', a: 75,  b: 65 },
            { y: '2010', a: 50,  b: 70 },
            { y: '2011', a: 80,  b: 85 },
            { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        grid: false,
        ykeys: ['a', 'b'],
        labels: ['Item A', 'Item B'],
        lineColors: ['#8CB4BC', '#538792'],
        gridTextColor : '#fff'
    });

    var barChart = Morris.Bar({
        element: 'barChart',
        data: [
            { y: '2006', a: 100, b: 90 },
            { y: '2007', a: 75,  b: 65 },
            { y: '2008', a: 50,  b: 40 },
            { y: '2009', a: 75,  b: 65 },
            { y: '2010', a: 50,  b: 40 },
            { y: '2011', a: 75,  b: 65 },
            { y: '2012', a: 100, b: 90 }
        ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        grid: false,
        labels: ['Item C', 'Item D'],
        barColors: ['#5EE1B1', '#3BC894'],
        gridTextColor : '#fff'
    });

    //Sparkline
    $('#visits').sparkline([15,19,20,22,33,27,31,27,19,30,21,10,15,18,25,9], {
        type: 'bar',
        barColor: '#FC8675',
        height:'35px',
        weight:'96px'
    });
    $('#balances').sparkline([220,160,189,156,201,220,104,242,221,111,164,242,183,165], {
        type: 'bar',
        barColor: '#65CEA7',
        height:'35px',
        weight:'96px'
    });

    //Timeline color box
    $('.timeline-img').colorbox({
        rel:'group1',
        width:"90%",
        maxWidth:'800px'
    });

    //Resize graph when toggle side menu
    $('.navbar-toggle').click(function()	{
        setTimeout(function() {
            donutChart.redraw();
            lineChart.redraw();
            barChart.redraw();

            $.plot($('#placeholder'), [init], options);
        },500);
    });

    $('.size-toggle').click(function()	{
        //resize morris chart
        setTimeout(function() {
            donutChart.redraw();
            lineChart.redraw();
            barChart.redraw();

            $.plot($('#placeholder'), [init], options);
        },500);
    });

    //Refresh statistic widget
    $('.refresh-button').click(function() {
        var _overlayDiv = $(this).parent().children('.loading-overlay');
        _overlayDiv.addClass('active');

        setTimeout(function() {
            _overlayDiv.removeClass('active');
        }, 2000);

        return false;
    });

    $(window).resize(function(e)	{

        //Sparkline
        $('#visits').sparkline([15,19,20,22,33,27,31,27,19,30,21,10,15,18,25,9], {
            type: 'bar',
            barColor: '#fa4c38',
            height:'35px',
            weight:'96px'
        });
        $('#balances').sparkline([220,160,189,156,201,220,104,242,221,111,164,242,183,165], {
            type: 'bar',
            barColor: '#92cf5c',
            height:'35px',
            weight:'96px'
        });

        //resize morris chart
        setTimeout(function() {
            donutChart.redraw();
            lineChart.redraw();
            barChart.redraw();

            $.plot($('#placeholder'), [init], options);
        },500);
    });

    $(window).load(function(e)	{

        //Number Animation
        var currentUser = $('#userCount').text();
        $({numberValue: 0}).animate({numberValue: currentUser}, {
            duration: 2500,
            easing: 'linear',
            step: function() {
                $('#userCount').text(Math.ceil(this.numberValue));
            }
        });

        var currentServerload = $('#serverloadCount').text();
        $({numberValue: 0}).animate({numberValue: currentServerload}, {
            duration: 2500,
            easing: 'linear',
            step: function() {
                $('#serverloadCount').text(Math.ceil(this.numberValue));
            }
        });

        var currentOrder = $('#orderCount').text();
        $({numberValue: 0}).animate({numberValue: currentOrder}, {
            duration: 2500,
            easing: 'linear',
            step: function() {
                $('#orderCount').text(Math.ceil(this.numberValue));
            }
        });

        var currentVisitor = $('#visitorCount').text();
        $({numberValue: 0}).animate({numberValue: currentVisitor}, {
            duration: 2500,
            easing: 'linear',
            step: function() {
                $('#visitorCount').text(Math.ceil(this.numberValue));
            }
        });

        setInterval(function() {
            var currentNumber = $('#userCount').text();
            var randomNumber = Math.floor(Math.random()*20) + 1;
            var newNumber = parseInt(currentNumber, 10) + parseInt(randomNumber, 10);

            $({numberValue: currentNumber}).animate({numberValue: newNumber}, {
                duration: 500,
                easing: 'linear',
                step: function() {
                    $('#userCount').text(Math.ceil(this.numberValue));
                }
            });
        }, 3000);

        setInterval(function() {
            var currentNumber = $('#visitorCount').text();
            var randomNumber = Math.floor(Math.random()*50) + 1;
            var newNumber = parseInt(currentNumber, 10) + parseInt(randomNumber, 10);

            $({numberValue: currentNumber}).animate({numberValue: newNumber}, {
                duration: 500,
                easing: 'linear',
                step: function() {
                    $('#visitorCount').text(Math.ceil(this.numberValue));
                }
            });
        }, 5000);
    });
});

