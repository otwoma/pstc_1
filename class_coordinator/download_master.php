<?php
/*Code by :: NEO*/
error_reporting(0);

include ('../_functions.php');
include ('../_variables.php');
//include ('_auth-registry.php');
//require_once '../packages/Classes/PHPWord/Autoloader.php';

//$phpWord = new \PhpOffice\PhpWord\PhpWord();

require_once '../packages/Classes/PHPWord.php';

$PHPWord = new PHPWord();

$extension = 'docx';

$ask=mysql_query("SELECT * FROM timetable");

$document = $PHPWord->loadTemplate('../assets/templates/timetable_2.docx');

$ref_labels[]='';
while($tt=mysql_fetch_assoc($ask)) {

    $unit_id = $tt['sub_id'];

    $unit_name = unit_code($unit_id);

    $unit_tag = unit_tag($unit_id);

    $unit_tag_label = read_unit_tag($unit_tag);

    $row = $tt['tbl_row'];
    $col = $tt['tbl_col'];

    $ref = $col.$row;
    $ref_tag = 'T'.$col.$row;

    $document->setValue($ref, $unit_name);
    $document->setValue($ref_tag, $unit_tag_label);

    $ref_labels[] = $ref;
    $ref_labels_tag[] = $ref_tag;

}

$timetable_refs = array_flatten($timetable_refs);
$timetable_refs_tag = array_flatten($timetable_refs_tag);


$blank = array_diff($timetable_refs,$ref_labels);
$blank_tag = array_diff($timetable_refs_tag,$ref_labels_tag);


$blank = array_flatten($blank);
$blank_tag = array_flatten($blank_tag);

foreach($blank as $blank_){

    $document->setValue($blank_,'');
}


foreach($blank_tag as $blank_tag_){

    $document->setValue($blank_tag_,'');
}


$file_name_x = 'Master Timetable - As at '. date("F jS Y (D) h.iA");
$file_x = 'timetable/'.$file_name_x.'.'.$extension;

$document->save($file_x);


header('Content-Description: File Transfer');
header('Content-type: application/force-download');
header('Content-Disposition: attachment; filename='.basename($file_x));
header('Content-Transfer-Encoding: binary');
header('Content-Length: '.filesize($file_x));
readfile($file_x);
unlink($file_x);



