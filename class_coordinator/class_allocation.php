<?php include '_header.class_coordinator.php';

$unit_ids=$_POST['unit_id'];
$instructor_id=$_POST['instructor_id'];
$class_ids=$_POST['class_id'];

?>

<style>
    label{
        font-size:14px;
        margin-right:7px;
    }
</style>

<div id="main-container">

    <div class="col-md-12">
        <h3>Allocation of Classes</h3>
        <hr style="border-top: dotted 1px; margin-top: -7px"/>


        <div align="center" class="panel-body">
            <form id="class_allocation_form" action="_actions.php" method="post" class="form-inline no-margin">


                <input type="hidden" name="action" value="class_allocation">

                <div class="right" style="float:left;">


                <div class="row">
                <div class="form-group col-lg-6">
                    <label for="unit_id">Select Unit</label>
                    <select id="unit_id" class="form-control cl_allocation" name="unit_id">
                        <?php echo class_allocation_unit_options();
                        ?>
                    </select>
                </div>
                </div>

                <div class="row">

                    <br>
                </div>


                <div class="row">
                <div class="form-group col-lg-6">
                    <label for="company_id">Company</label>
                    <select id="company_id" class="form-control cl_allocation" name="company_id">
                        <?php echo company_options();
                        ?>
                    </select>
                </div>
                </div>


                    <div class="row">

                        <br>
                    </div>



                    <div class="row" id="class_allocation_view">


                    </div>



                    <div class="row">
                        <div class="form-group col-lg-5 col-lg-offset-2">
                            <button id="saveData" type="button" class="btn btn-primary"><i class="fa fa-send"></i> Save Class Allocation</button>
                        </div>
                    </div>


                </div>

            </form>
        </div>


    </div>
</div>

</div>

<script>
    $(".class_allocation_mu").addClass('active');
</script>

<?php include '../_footer.php';?>


<script>


    $(document).on("click", "#saveData", function () {

    $.post('_actions.php', $('#class_allocation_form').serialize(),

            function (response) {
                alert(response);
            });

    });


    $(document).on("click", ".cl_allocation", function () {


        load_allocation_view();

    });

    function load_allocation_view(){

        var cl_view = $("#class_allocation_view");

        $.post('_class_allocation_view.php', $('#class_allocation_form').serialize(),
            function (response) {

                cl_view.html(response);
            });

    }

    load_allocation_view();

</script>
