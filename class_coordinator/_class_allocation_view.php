<?php
//include ('_auth-cc.php');
include ('../_functions.php');
include ('../_variables.php');

$unit_id = $_REQUEST['unit_id'];
$company_id = $_REQUEST['company_id'];

$unit_name = unit_name($unit_id);
$company_label = company_name($company_id);

?>

<div class="col-lg-11 col-lg-offset-1">


    <div class="row">

        <div class="col-lg-12">



            <h5>
                <strong><?php echo $unit_name.' </br>  </br></strong> ('.$company_label.')';?>
            </h5>


        </div>

    </div>


                        <table class="table table-striped table-hover col-lg-12">

                            <tr class="bold" style="font-weight:bold; background-color: #E9E9E9;">
                                <td width="20%"> <h5 class="bold">CLASS</h5> </td>
                                <td width="40%"> <h5 class="bold">INSTRUCTOR</h5>  </td>
                            </tr>


                            <?php

                            $select_classes = m("SELECT * FROM classes WHERE status = 1");

                            while($class=mray($select_classes)){

                                $class_id = $class['id'];
                                $class_label = $class['name'];


                                $allocated_instructor = fetch_class_allocation_instructor($class_id,$unit_id,$company_id);

                                ?>


                                <input type="hidden" name="allocation_class[]" value="<?php echo $class_id;?>"/>

                                <tr>
                                    <td class="bold"> <?php echo $class_label;?>  </td>
                                    <td>
                                        <select class="form-control" name="allocation_staff[]" id="allocation_staff">

                                            <?php

                                            echo class_allocation_staff_options($unit_id,$allocated_instructor);

                                            ?>

                                        </select>
                                    </td>
                                </tr>

                            <?php } ?>

</table>

</div>


