<?php include'_header.class_coordinator.php';
?>
    <div id="main-container">

        <link rel="stylesheet" href="../assets/css/time_table_style.css?v=2" type="text/css" media="screen"/>

        <div class="padding-md">
            <div class="panel panel-default table-responsive">

                <div align="center" class="panel-body">
                    <form action="" method="get" class="form-inline no-margin">
                        <div class="form-group"><span class="bold font-14">Select Staff:</span>
                            <label class="sr-only">Staff</label>
                            <select class="form-control inline-block" name="staff_id">
                                <option value="">Staff</option>
                                <?php
                                echo staff_options();
                                ?>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-sm btn-primary">See Schedule</button>
                    </form>
                </div>


                    <?php

                    $staff_id=($_REQUEST['staff_id']);

                    time_table_staff_display($staff_id)?>


            </div>
        </div>
    </div>
    <script> $(".tims_mu").addClass('active');$(".staff_schedule_mu").addClass('active');</script>

<?php include'../_footer.php';?>