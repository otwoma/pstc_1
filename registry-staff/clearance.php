<?php include '_header.reg-staff.php';

$perm = $_GET['p'];

$perm_label = read_registry_staff_permission_label($perm);
$perm_icon = read_registry_staff_permission_icon($perm);

 $perms = read_registry_staff_permissions_array($_SESSION['_aid']);


$cleared_recruits = get_cleared_recruits($perm);


if(in_array($perm,$perms)){


}else{

    header("Location:../logout.php");
    echo "<script type='text/javascript'>  window.location='../logout.php'; </script>";
        exit;
}

?>
<div id="main-container">

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>
			<div class="padding-md">
				<div class="panel panel-default table-responsive">
					<div class="padding-sm font-16">
					<i class="fa <?php echo $perm_icon;?>"></i>	<?php echo $perm_label;?>
                        <a style="margin-right:10px;" href="clearance_status_report.php?p=<?php echo $perm?>" role="button" class="btn btn-primary btn-sm pull-right"><span class="fa fa-download"></span>  Download <?php echo $perm_label?> Status</a>

                    </div>

                    <div class="seperator"></div><div class="seperator"></div>
					<table class="table table-striped" id="responsiveTable">
						<thead>
							<tr>
                                <th width="" align="left"><span class=""></span>Recruit Name</th>
                                <th width="" align="left"><span class=""></span>Sto. Number</th>
                                <th width="" align="left"><span class=""></span>Gender</th>
                                <th width="" align="left"><span class=""></span>National ID</th>
                                <th width="" align="left"><span class=""></span><?php echo $perm_label?> Status</th>
                                <th><span class=""></span></th>
							</tr>
						</thead>
						<tbody>
                        <?php
                        $list = "SELECT * FROM recruits WHERE status = 1";
                        $list_query= $d->q($list);
                        while($list_result = msoc($list_query)){
                            $sto_number=trailing_zeros($list_result['sto_number']);
                            $national_id=$list_result['national_id'];
                            $gender=$list_result['gender'];
                            $gender= read_gender($gender);
                            $recruit_id = $list_result['recruit_id'];
                            $platoon = platoon_name($list_result['platoon_id']);


                            $surname=$list_result['surname'];
                            $first_name=$list_result['first_name'];
                            $othername=$list_result['othername'];

                            $full_name=$surname.' '.$first_name.' '.$othername;
                            if($gender=='Male'){$bclas='butn-navy';}else{$bclas='butn-blue';}
                            $made_title=$full_name.' - '.$sto_number;

                            $clearance_state =detect_clearance($recruit_id,$perm);


                            ?>
                            <tr title="<?php echo $made_title;?>">
                                <td><?php echo $full_name;?></span></td>
                                <td align="left"><?php echo $sto_number;?></td>
                                <td align="left"><?php echo $gender?></td>
                                <td align="left"><?php echo $national_id?></td>

                                    <?php
                                    if($clearance_state==0){

                                        $label_text = 'Not Cleared';
                                        $label_color = 'label-danger';

                                        ?>
                                <td align="left">  <span class="font-15 label <?php echo $label_color;?>" style="background-color: #626262;"> <i class="fa fa-times"></i> <?php echo $label_text;?> </span> </td>

        <td align="left">
            <a data-recruit_id="<?php echo $recruit_id;?>" data-name="<?php echo $full_name;?>" data-sto="<?php echo $sto_number;?>" class='clear_recruit' href='#clearModal' role='button' data-toggle='modal'>  <button class="btn btn-success"> <i class="fa fa-check"></i> Clear Student </button> </a>
        </td>

                                    <?php


                                    }else{
                                        $label_text = 'Cleared';
                                        $label_color = 'label-success';
?>

                                <td align="left">   <span class="font-15 label <?php echo $label_color;?>"> <i class="fa fa-checkmark"></i> <?php echo $label_text;?> </span></td>

                            <td align="left">

                            </td>

                                        <?php
                                    }


                                    ?>

                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
					</table>
				</div>
			</div>
		</div>




<div class="modal fade" id="clearModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #356837;color:#fff;">
                <button type="button" class="close" style="color:#fff;" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4> <?php echo $perm_label;?> Confirmation</h4>
                <span class="fa fa-chevron-right"> <span id="item_labelG" class="font-14"></span></span>
                You're about to clear a recruit for <?php echo $perm_label;?>
            </div>
            <div class="modal-body">

                <br>
                <span style="font-size:19px;">Are you sure that <span style="" id="modal_recruit_name"></span> of Sto No. <span style="" id="modal_recruit_sto"></span> is fit for <?php echo $perm_label;?> ?
</span>


                <br>
                <br>

                <small>Hint: If yes click proceed </small>
                <br>
                <br>

               </div>
            <div class="modal-footer">

                <form action="_actions.php" method="POST" enctype="multipart/form-data">

                    <input type="hidden" name="action" value="clearance">
                    <input type="hidden" value="0" name="recruit_id" id="target_recruit_id">
                    <input type="hidden" value="<?php echo $perm;?>" name="clearance_type" id="target_clearance_type">

                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-send"></i> Yes, Proceed </button>


                </form>
            </div>
        </div>
    </div>

</div>




<script> $(".clearance_<?php echo $perm;?>").addClass('active');

    $(document).on("click", ".clear_recruit", function () {

        var target = $(this);

        var name = target.data('name');
        var sto = target.data('sto');
        var recruit_id = target.data('recruit_id');

        $('#modal_recruit_name').html(name);
        $('#modal_recruit_sto').html(sto);

        $('#target_recruit_id').val(recruit_id);

    });


</script>

<?php include '../_footer.php'; ob_end_flush();?>
