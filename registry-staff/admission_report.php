<?php
include "../_functions.php";
include "../_variables.php";
//include "../packages/classes/PHPExcel.php";
require_once dirname(__FILE__) . '/../packages/Classes/PHPExcel.php';

$class_id=mysql_real_escape_string($_REQUEST['fx']);
$defa_excel_expo_format='xlsx';
$expot_cateogo='Fee Balance';
$exofile_name='dsd'.' '.$expot_cateogo;


$doc_subject=$exofile_name;
$doc_description=$exofile_name;
$keywords=$exofile_name;
$doc_category=$expot_cateogo;
$doc_title=$exofile_name;
$last_mod_author=$software_label;
$doc_creator=$software_label.'>'.$school_name;


$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($doc_creator)
    ->setLastModifiedBy($last_mod_author)
    ->setTitle($doc_title)
    ->setSubject($doc_subject)
    ->setDescription($doc_description)
    ->setKeywords($keywords)
    ->setCategory($doc_category);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getDefaultStyle()->getFont()
    ->setName('Segoe UI Symbol')
    ->setSize(11);

$class_listex_titl= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 11,
    ));


$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue('A1',strtoupper($exofile_name).' as at '.date("Y:"));



$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

$pos_handler=3;
$title_po=$pos_handler-1;
$objPHPExcel->getActiveSheet()->setCellValue('B'.($title_po),'Name');
$objPHPExcel->getActiveSheet()->setCellValue('C'.($title_po),'Sto. Number');
$objPHPExcel->getActiveSheet()->setCellValue('D'.($title_po),'Balance');

$query_recruits = m("SELECT * FROM recruits WHERE status =1");


while($recruit=msoc($query_recruits)){

    $full_name=$recruit['surname'].' '.$recruit['first_name'].' '.$recruit['other_name'];


    $objPHPExcel->getActiveSheet()->setCellValue('A'.$pos_handler,'');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler,$full_name);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$pos_handler,'');

    $objPHPExcel->getActiveSheet()->getStyle('B'.$pos_handler)->getAlignment()->applyFromArray(
        array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
    );

    $pos_handler=$pos_handler+1;
}

//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);
$objPHPExcel->getActiveSheet()
    ->getColumnDimension('C')
    ->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);



//$objPHPExcel->getActiveSheet()->setPrintGridlines(TRUE);




$objPHPExcel->getActiveSheet()->setTitle('FeeBalances');


$objPHPExcel->setActiveSheetIndex(0);


$exofile_name=$exofile_name.'.'.$defa_excel_expo_format;

if($defa_excel_expo_format=='xlsx'){
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

}elseif($defa_excel_expo_format=='xls'){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}

$objWriter->save('php://output');
exit;
