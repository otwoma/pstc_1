<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 6/8/2017
 * Time: 3:30 PM
 */

include'_header_store.php';

$item_rcvd = cleaN($_POST['motion_tg']);
if($item_rcvd!=''){
    store_motion_back($item_rcvd);
}
?>
    <div id="main-container">
        <script type="text/javascript" charset="utf-8">
            $(function() {
                $('#receivingTable').dataTable();
            } );
        </script>
        <div class="padding-md">
            <div class="panel panel-default table-responsive">
                <div class="padding-sm font-16">
                    <span class="fa fa-external-link"></span> Items Given Out

                </div>
                <div class="seperator"></div><div class="seperator"></div>
            </div>


            <table class="table table-striped" id="receivingTable">
                <thead>
                <tr>
                    <th width="" align="left"><span class=""></span>Name</th>
                    <th width="" align="left"><span class=""></span>#</th>
                    <th width="" align="left"><span class=""></span>ID</th>
                    <th width="" align="left"><span class=""></span>Item Label</th>
                    <th width="" align="left"><span class=""></span>Pieces</th>
                    <th width="" align="left"><span class=""></span>Time Taken</th>
                    <th width="" align="left"><span class=""></span></th>

                </tr>
                </thead>
                <tbody>
                <?php

                //$list = "SELECT batch,id,label,type,cnd,date_added FROM store_items ORDER BY id ASC";
                $list = "SELECT move_id,move_note,holder,move_time,holder_group FROM store_motion WHERE move_back  = ''";
                $list_query = mysql_query($list);
                $coun=0;
                while($list_result = mysql_fetch_array($list_query)){
                    $themd=$list_result['move_id'];
                    $move_note = $list_result['move_note'];
                    $holder_id = $list_result['holder'];
                    $time_taken = $list_result['move_time'];
                    $item_holder = item_holder_details($holder_id,$list_result['holder_group']);
                    $item_lid= fetch_item_id($themd);
                    $item_label = get_item_label($item_lid);
                    $nitems_b = count_items_taken($themd);
                    $trake_gr=$item_holder[1];
                    $aker_nae= $item_holder[0];;
                    ?>
                    <tr>
                        <td align="left"><?php echo $aker_nae;?></td>
                        <td align="left"><?php echo $trake_gr;?></td>
                        <td align="left"><?php echo $holder_id?></td>
                        <td align="left"><?php echo $item_label;?></td>
                        <td align="left"><?php echo $nitems_b;?></td>
                        <td align="left"><?php echo $time_taken;?></td>
                        <td align="center"><a class='item_receive' href='#storeReceive' data-ti="<?php echo $time_taken;?>" data-taker="<?php echo $aker_nae.' ('.$trake_gr.')';?>" data-ni="<?php echo $nitems_b.' piece'.plu($nitems_b);?>" data-iteml="<?php echo $item_label;?>" data-moveid="<?php echo $themd;?>" role='button' data-toggle='modal'><button class='butn butn-blue'><span class='fa fa-level-down'></span> Receive</button></a></td>

                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>



    <div class="modal fade" id="storeReceive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-nice-dark2 white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <span id="item_labelR" class="font-14 bold"></span></span>
                    </br> </br>
                    <span id="exp_items" class="font-14 bold margin-sm"></span>
                </div>

                <div class="modal-body">
                    <form id="rcv_inform" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadR" name="give_item_lead"/></span>

                        <div class="form-group bold">
                            Given to: <span class="font-14 bold" id="recv_from"></span>
                        </div>

                        <div align="right" class="ital">Given <span id="gvn_itGo"></span></div>

                        <input hidden class="hidden" type="text" name="motion_tg" id="motion_tg" required/>


                        <div class="form-group">
                            <span class="text-wrap bold" id="move_note_xq"></span>
                        </div>


                </div>
                <div class="modal-footer">
                    <button class="btn butn-white btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn butn-navy btn-sm white">Receive</button>
                </div>
            </div>
        </div>
        </form>
    </div>




    <script> $(".store_mu").addClass('active');

        $("body").on('click','.item_receive',function(){
            var motion_id= $(this).data("moveid");
            var motion_item= $(this).data("iteml");
            var i_n= $(this).data("ni");
            var mvtime= $(this).data("ti");
            $("#item_labelR").html(motion_item);
            $("#exp_items").html(i_n);
            $("#motion_tg").val(motion_id);

            $("#recv_from").html($(this).data("taker"));

            $.post('_ajax.php', 'timeago='+mvtime,
                function (time_ago) {
                    $('#gvn_itGo').html(time_ago)
                });
            $.post('../_ajax.php', 'store_item_note='+motion_id,
                function (mnote) {
                    $('#move_note_xq').html(mnote)
                });

        });



        $("body").on('change','#gvtaker_sel',function(){
            $("#taker_").html('');
            $("#taken_id").val('');
            var tak = $(this).val();
            var label = $("#taker_i");
            if(tak==1){label.html("Student ID.")}
            if(tak==2){label.html("Staff ID.")}
        });

        $('#taken_id').on("keyup",function (tnane) {
            $("#taker_").html('');
            $.ajax({
                url: '../_ajax.php',
                type: 'POST',
                data: $('#gve_outfom').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;
                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });
        });
    </script>
<?php include'../_footer.php';?>