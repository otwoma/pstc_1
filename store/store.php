<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 6/8/2017
 * Time: 3:12 PM
 */

include'_header_store.php';

$item_rcvd = cleaN($_POST['motion_tg']);
if($item_rcvd!=''){
    store_motion_back($item_rcvd);
}

$item_label=strtoupper(mysql_real_escape_string($_POST['item_label']));
/*$item_condition=mysql_real_escape_string($_POST['item_condition']);*/
$items=cleaN($_POST['number_of_items']);
$item_type =cleaN($_POST['item_type']);
$threshold = cleaN($_POST['threshold']);
$unit = cleaN($_POST['unit']);
$items=trim($items);
$stock_batch=rand_string(4).uniqid().$items.rand_letters(5);

if($item_type!='' && isset($item_label) && $items>0){
    $i=0;
    do{
        $ipar=$i+1;
        $iden=$stock_batch.'_'.($ipar);
        m("INSERT INTO store_items SET iden = '".$iden."',batch='".$stock_batch."',type = '".$item_type."',label='".$item_label."', threshold = '".$threshold."', unit = '".$unit."'");
        $i=$ipar;
    }while($i<$items);
}else{}

$item_taker=cleaN($_POST['taker_t']);
$item_taken_by=cleaN($_POST['taker_id']);
$givetran_note = cleaN($_POST['gtran_note']);
$item_fo_gid=cleaN($_POST['give_item_lead']);
$give_n_items=cleaN($_POST['given_itms']);
if(isset($item_taker) && $item_taken_by !='' && isset($item_fo_gid)){
    $giid=store_get_item_id_give($item_fo_gid,$give_n_items);
    $mid_tranid = rand_letters(3).$stock_batch.time();
    foreach($giid as $item_d){
        m("INSERT INTO store_moves SET mid='".$mid_tranid."',item_id = '".$item_d."'");
        m("UPDATE store_items SET state = 0 WHERE id = '".$item_d."'");

    }
    m("INSERT INTO store_motion SET holder = '".$item_taken_by."',move_id = '".$mid_tranid."',holder_group  ='".$item_taker."',move_note='".$givetran_note."'");
}
?>
    <div id="main-container">
        <script type="text/javascript" charset="utf-8">
            $(function() {
                $('#responsiveTable').dataTable();
                $('#receivingTable').dataTable();
            } );
        </script>

        <ul class="tab-bar grey-tab">
            <li class="protab">
                <a href="#items_in" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-cart-plus fa-2x"></i>
                    </span>
                    Items in Store
                </a>
            </li>

            <li class="protab">
                <a href="#items_out" data-toggle="tab">
                    <span class="block text-center">
                        <i class="fa fa-life-ring fa-2x"></i>
                    </span>
                    Items Out
                </a>
            </li>
        </ul>

        <div class="padding-md">
            <div class="padding-sm font-16">
                Store Items
                <a href="#newStoreitem" role="button" data-toggle="modal" class="btn btn-info btn-small" style="margin-left: 900px"><span class="fa fa-plus"></span> Add Item</a>
            </div>
            <div class="panel panel-default table-responsive">
                <div class="tab-content">
                    <div class="tab-pane fade in active " id="items_in">
                        <div class="panel panel-default table-responsive">
                            <div class="seperator"></div><div class="seperator"></div>
                            <table class="table table-striped" id="responsiveTable">
                                <thead>
                                <tr>
                                    <th width="" align="left"><span class=""></span>Label</th>
                                    <th width="" align="left"><span class=""></span>Type</th>
                                    <th width="" align="left"><span class=""></span>Items In</th>
                                    <th width="" align="left"><span class=""></span>Items Out</th>
                                    <th width="20%" align="center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                //$list = "SELECT batch,id,label,type,cnd,date_added FROM store_items ORDER BY id ASC";
                                $list = "SELECT iden,label,type,date_added,id FROM store_items GROUP BY label";
                                $list_query = mysql_query($list);
                                $coun=0;
                                while($list_result = mysql_fetch_array($list_query)){
                                    $therd=$list_result['id'];
                                    $ilabl=$list_result['label'];
                                    $utem=ucwords($ilabl);
                                    $nin=count_items_in($ilabl);
                                    $nout=count_items_out($ilabl);
                                    ?>
                                    <tr>
                                        <td align="left"><?php echo $utem;?></td>
                                        <td align="left"><?php echo ($list_result['type'])?></td>
                                        <td align="left"><?php echo $nin;?></td>
                                        <td align="left"><?php echo $nout;?></td>
                                        <td align="center"><?php if($nin!=0){echo "<a class='item_give' href='#storeGive' data-label='$utem' data-maxi='$nin' data-lead='$therd' role='button' data-toggle='modal'><button class='btn btn-primary'><span class='fa fa-mail-forward'></span> Give</button></a>";}else{echo "<button class='butn butn-red'> All Out</button>";}?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="items_out">
                        <div class="panel panel-default table-responsive">
                            <div class="separator"></div><div class="separator"></div>
                            <table class="table table-striped" id="receivingTable">
                                <thead>
                                <tr>
                                    <th width="" align="left"><span class=""></span>Name</th>
                                    <th width="" align="left"><span class=""></span>#</th>
                                    <th width="" align="left"><span class=""></span>ID</th>
                                    <th width="" align="left"><span class=""></span>Item Label</th>
                                    <th width="" align="left"><span class=""></span>Pieces</th>
                                    <th width="" align="left"><span class=""></span>Time Taken</th>
                                    <th width="" align="left"><span class=""></span></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                //$list = "SELECT batch,id,label,type,cnd,date_added FROM store_items ORDER BY id ASC";
                                $list = "SELECT move_id,move_note,holder,move_time,holder_group FROM store_motion WHERE move_back  = ''";
                                $list_query = mysql_query($list);
                                $coun=0;
                                while($list_result = mysql_fetch_array($list_query)){
                                    $themd=$list_result['move_id'];
                                    $move_note = $list_result['move_note'];
                                    $holder_id = $list_result['holder'];
                                    $time_taken = $list_result['move_time'];
                                    $item_holder = item_holder_details($holder_id,$list_result['holder_group']);
                                    $item_lid= fetch_item_id($themd);
                                    $item_label = get_item_label($item_lid);
                                    $nitems_b = count_items_taken($themd);
                                    $trake_gr=$item_holder[1];
                                    $aker_nae= $item_holder[0];;
                                    ?>
                                    <tr>
                                        <td align="left"><?php echo $aker_nae;?></td>
                                        <td align="left"><?php echo $trake_gr;?></td>
                                        <td align="left"><?php echo $holder_id?></td>
                                        <td align="left"><?php echo $item_label;?></td>
                                        <td align="left"><?php echo $nitems_b;?></td>
                                        <td align="left"><?php echo $time_taken;?></td>
                                        <td align="center"><a class='item_receive' href='#storeReceive' data-ti="<?php echo $time_taken;?>" data-taker="<?php echo $aker_nae.' ('.$trake_gr.')';?>" data-ni="<?php echo $nitems_b.' piece'.plu($nitems_b);?>" data-iteml="<?php echo $item_label;?>" data-moveid="<?php echo $themd;?>" role='button' data-toggle='modal'><button class='butn butn-blue'><span class='fa fa-level-down'></span> Receive</button></a></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="newStoreitem">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Adding New Store Item</h4>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" enctype="multipart/form-data">

                        <div class="form-group">
                            <label>Item Type</label></br>
                            <select class="form-control" style="margin-left:0px;" name="item_type">
                                <?php echo store_item_type()?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Item Label</label></br>
                            <input placeholder="eg. Tennis Ball" type="text" name="item_label" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Number of Items</label></br>
                            <input placeholder="15" type="text" name="number_of_items" value="1" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Threshold Quantity</label></br>
                            <input placeholder="Threshold Quantity (Optional)" type="number" name="threshold" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Unit of Measure</label></br>
                            <select name="unit" id="" class="form-control">
                                <?php echo store_unit_options()?>
                            </select>
                        </div>

                        <!-- <div class="form-group">
                            <label>Condition</label></br>
                            <select class="form-control" style="margin-left:0px;" name="item_condition">
                                <?php /*echo store_item_condition()*/?>
                            </select>
                        </div>-->
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>

    <div class="modal fade" id="storeGive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Giving Out</h4>
                    <span class="fa fa-chevron-right"> <span id="item_labelG" class="font-14"></span></span>

                </div>
                <div class="modal-body">
                    <form id="gve_outfom" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadG" name="give_item_lead"/>
                        <input hidden="" id="pmax" name="pimax"/>
                        </span>

                        <div class="form-group">
                            <label>Pieces</label>
                            <input id="pitem_c" onchange="handleChange(this);" style="width: 55px;" value="1" type="text" name="given_itms" class="form-control" required>
                            <script>
                                $('#pitem_c').on("keyup",function (mtc) {
                                    var max = $("#pmax").val();
                                    var mvax = $(this).val();
                                    if(mvax > max){
                                        alert('Currently the maximum number of pieces that can be given is '+max);
                                        $(this).val(max);
                                    }
                                });
                            </script>
                        </div>

                        <div class="form-group">
                            <label>Giving</label></br>
                            <select id="gvtaker_sel" class="form-control" style="width: 300px;" name="taker_t">
                                <?php echo store_give_out_options()?>
                            </select>
                        </div>


                        <div class="form-group">
                            <label id="taker_i">Student ID.</label> <label id="taker_"></label>
                            <input style="font-size: 16px;" id="taken_id" type="text" name="taker_id" class="form-control" required>

                        </div>


                        <div class="form-group">
                            <label>Note: </label>
                            <textarea class="form-control input-sm" name="gtran_note" style="height: 55px;"></textarea>
                        </div>

                        <span id="taker_nameclue"></span>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Submit</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <div class="modal fade" id="storeReceive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-nice-dark2 white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <span id="item_labelR" class="font-14 bold"></span></span>
                    </br> </br>
                    <span id="exp_items" class="font-14 bold margin-sm"></span>
                </div>

                <div class="modal-body">
                    <form id="rcv_inform" action="" method="POST" enctype="multipart/form-data">
                        <span class="hidden"><input hidden="" id="item_leadR" name="give_item_lead"/></span>

                        <div class="form-group bold">
                            Given to: <span class="font-14 bold" id="recv_from"></span>
                        </div>

                        <div align="right" class="ital">Given <span id="gvn_itGo"></span></div>

                        <input hidden class="hidden" type="text" name="motion_tg" id="motion_tg" required/>


                        <div class="form-group">
                            <span class="text-wrap bold" id="move_note_xq"></span>
                        </div>
                </div>
                <div class="modal-footer">
                    <button class="btn butn-white btn-sm" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn butn-navy btn-sm white">Receive</button>
                </div>
            </div>
        </div>
        </form>
    </div>


    <script> $(".inventory_mu").addClass('active');

        $("body").on('click','.item_give',function(){
            var ilabel= $(this).data("label");
            var glead= $(this).data("lead");
            var mx= $(this).data("maxi");
            $("#item_labelG").html(ilabel);
            $("#item_leadG").val(glead);
            $("#pmax").val(mx);
        });
        function handleChange(input) {
            var qax=  $("#pmax").val();
            if (input.value < 0) input.value = 0;
            if (input.value > qax) input.value = qax;
        }

            $("body").on('click','.item_receive',function(){
                var motion_id= $(this).data("moveid");
                var motion_item= $(this).data("iteml");
                var i_n= $(this).data("ni");
                var mvtime= $(this).data("ti");
                $("#item_labelR").html(motion_item);
                $("#exp_items").html(i_n);
                $("#motion_tg").val(motion_id);

                $("#recv_from").html($(this).data("taker"));

                $.post('_ajax.php', 'timeago='+mvtime,
                    function (time_ago) {
                        $('#gvn_itGo').html(time_ago)
                    });
                $.post('../_ajax.php', 'store_item_note='+motion_id,
                    function (mnote) {
                        $('#move_note_xq').html(mnote)
                    });

            });


        $("body").on('change','#gvtaker_sel',function(){
            $("#taker_").html('');
            $("#taken_id").val('');
            var tak = $(this).val();
            var label = $("#taker_i");
            if(tak==1){label.html("Student ID.")}
            if(tak==2){label.html("Staff ID.")}
        });

        $('#taken_id').on("keyup",function (tnane) {
            $("#taker_").html('');
            $.ajax({
                url: '../_ajax.php',
                type: 'POST',
                data: $('#gve_outfom').serialize(),
                success: function(cnamef){
                    var len = cnamef.length;
                    if(len!=2){
                        var nabel = '('+cnamef+')';
                        $("#taker_").html(nabel);
                    }
                }
            });
        });
    </script>
<?php include'../_footer.php';?>