<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/12/2017
 * Time: 2:58 PM
 */

include '_header_store.php';

?>
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#responsiveTable').dataTable();
        } );
    </script>

<div id="main-container">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <i class="fa fa-server"></i> Purchase Request Records
                </h4>
            </div>
        </div>
    </div>
    <div class="col-md-12 ">
        <div class="panel panel-default table-responsive">
            <div class="padding-sm font-16">
                Listing Item Categories
            </div>
            <table class="table table-striped" id="responsiveTable">
                <thead>
                <tr>
                    <th width="" align="left">Order No.</th>
                    <th width="" align="center"><span class=""></span>Item Description</th>
                    <th width="" align="right"><span class=""></span>Quantity Procured</th>
                    <th width="" align="right"><span class=""></span>Unit Cost</th>
                    <th width="" align="right"><span class=""></span>Cost</th>
                    <th width="" align="right"><span class=""></span>Date</th>
                    <th width="" align="right"><span class=""></span></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $list = "SELECT prform_id, description, qty_received, value, qty_cost, created_at FROM prform_data ORDER BY prform_id ASC";
                $list_query = mysql_query($list);
                while($list_result = mysql_fetch_array($list_query)){
                    ?>
                    <tr>
                        <td align="left"><?php echo $id=($list_result['prform_id'])?></td>
                        <td align="left"><?php echo ucwords($list_result['description']);?></td>
                        <td align="left"><?php echo $list_result['qty_received']?></td>
                        <td align="left"><?php echo $list_result['qty_cost']?></td>
                        <td align="left"><?php echo $list_result['value']?></td>
                        <td align="left"><?php echo $list_result['created_at']?></td>
                        <td><a href="purchase_form.php?cid=<?php echo $id;?>" class="btn btn-success"><i class="fa fa-file-word-o"></i> Download Form</a></td>
                    </tr>
                    <?php
                }
                ?></tbody>
            </table>
        </div>
    </div>
</div>


    <script>$('.purchases_mu').addClass('active')</script>
<?php include'../_footer.php'?>