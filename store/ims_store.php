<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 6/1/2017
 * Time: 11:35 AM
 */

include'_header_store.php';

?>

<div id="main-container">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <i class="fa fa-envelope-open-o"></i> Received Store Requests
                </h4>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body no-padding">
                <div class="tab-pane fade in active" id="store_mes">
                    <?php echo ims_display_store($tid)?>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="openIMS">
        <div class="modal-dialog">
            <div class="modal-content" id="ims_read">
            </div>
        </div>
    </div>

<script> $(".storemes_mu").addClass('active');
    function reduce_ims(){
        var cnx=$("#cnt_ms_urd");
        var ccnx=$(".cnt_ms_urd");
        var cnxx=cnx.text();
        var ncnxx=+cnxx-1;
        cnx.html('');
        ccnx.text(ncnxx);
        if(ncnxx==0){$(".tohidethis").hide()};
    }
    $(".xoims").on('click',function () {
        var dx=$(this).data("wim");
        $.post('../_ajax', 'read_ims='+dx,
            function (uil) {
                $("#ims_read").html(uil);
            });
    });
    $(".x_ur").on('click',function () {
        var rx=$(this).data("wim");
        var cxx=$(this);
        $.post('../_ajax', 'ims_mark='+rx+'&xs_gims=<?php echo urlencode($tid);?>',
            function (xmx) {
                $(cxx).find('.red').css({"color":"grey"});
                $(cxx).css({"border-left":"3px solid #B1B0B0","border-right":"3px solid #B1B0B0"});
            });
        reduce_ims();
    });
</script>
<?php include'../_footer.php';?>