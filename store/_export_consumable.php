<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/9/2017
 * Time: 9:08 AM
 */

require_once dirname(__FILE__) . '../packages/Classes/PHPExcel.php';

$consumable_id=sani($_REQUEST['cid']);

$format=$_REQUEST['format'];
$formats=array('xls','xlsx','pdf');
if(in_array($format,$formats)){
    $defa_excel_expo_format=$format;
}else{
    $defa_excel_expo_format='xlsx';

}

$consumable_label=consumable_label($consumable_id);
//$consumable_label=str_replace('| ','(',$consumable_label).')';

//die($consumable_label);
$expot_cateogo='Consumable Report';
$exofile_name=$expot_cateogo.' for '.$consumable_label;


$doc_subject=$exofile_name;
$doc_description=$exofile_name;
$keywords=$exofile_name;
$doc_category=$expot_cateogo;
$doc_title=$exofile_name;
$last_mod_author=$software_label;
$doc_creator=$software_label;


$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator($doc_creator)
    ->setLastModifiedBy($last_mod_author)
    ->setTitle($doc_title)
    ->setSubject($doc_subject)
    ->setDescription($doc_description)
    ->setKeywords($keywords)
    ->setCategory($doc_category);

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getDefaultStyle()->getFont()->setSize(13);
$class_listex_titl= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 14,
    ));
$total_style= array(
    'font'  => array(
        'bold'  => true,
        'size'  => 13,
    ));
$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);

$objPHPExcel->getActiveSheet()->getStyle('C')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);

$objPHPExcel->getActiveSheet()->getStyle('D')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle('E')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
);

/*$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue('A1',strtoupper($exofile_name).' as at '.date("Y:"));



$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);*/

/*
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('School logo');
$objDrawing->setDescription('School logo image');
$objDrawing->setPath($school_logo);
$objDrawing->setHeight(150);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(10);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
$pos_handler=1;

$main_title='A'.$pos_handler;
$asset_model=get_asset_model_id($asset_id);
$asset_model=get_model_label($asset_model);
if(strlen($asset_model)>3){
    $model_show='(Model: '.$asset_model.')';
}else{
    $model_show='';
}
$nowx=date("F jS, Y (D) H:i");
$intro_text="Consumable Report for: \r\r\n"."$consumable_label "." \r\r"."As at ".$nowx;
$merge_this_one=$main_title.':'.'E'.$pos_handler;
$objPHPExcel->getActiveSheet()->mergeCells($merge_this_one);
$objPHPExcel->getActiveSheet()->getStyle($main_title)->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue($main_title,$intro_text);
$objPHPExcel->getActiveSheet()->getRowDimension($pos_handler)->setRowHeight(20);


$pos_handler=3;
$title_po=$pos_handler-1;
$xu='A'.($title_po);
$c1='B'.($title_po);
$objPHPExcel->getActiveSheet()->getStyle($title_po)->applyFromArray($class_listex_titl);
$objPHPExcel->getActiveSheet()->setCellValue($xu,'Date');
$objPHPExcel->getActiveSheet()->setCellValue($c1,'User');
$objPHPExcel->getActiveSheet()->setCellValue('C'.($title_po),'Quantity');
$objPHPExcel->getActiveSheet()->setCellValue('D'.($title_po),'Cumulative');
$objPHPExcel->getActiveSheet()->setCellValue('E'.($title_po),'');


$list = "SELECT * FROM consumable_movement WHERE consumable_id = '".$consumable_id."' ORDER BY id ASC";
$list_query = m($list);
$total=0;
$ti=0;
$ag=0;
while($list_result = mysql_fetch_array($list_query)){
    $user_id=$list_result['user_id'];
    $usern=get_user_full_name($user_id);
    $ttt_time=$list_result['stamp'];

    $quantity=$list_result['quantity_taken'];

    $exc=explode(' ',$ttt_time);

    $dat=trim($exc[0]);



    $ag=$ag+$quantity;

    $tr_pos='C'.$pos_handler;
    $src_pos='D'.$pos_handler;
    $amnt_pos='E'.$pos_handler;
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$pos_handler,good_date($dat));
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$pos_handler, $usern);
    $objPHPExcel->getActiveSheet()->setCellValue($tr_pos, $quantity);
    $objPHPExcel->getActiveSheet()->setCellValue($src_pos, $ag);
    $objPHPExcel->getActiveSheet()->setCellValue($amnt_pos, $ttt_time);


    $objPHPExcel->getActiveSheet()->getRowDimension($pos_handler)->setRowHeight(18);
    $pos_handler=$pos_handler+1;

}


$total_pos_term='B'.$pos_handler;
$total_pos='E'.$pos_handler;

$merge_this=$total_pos_term.':'.'D'.$pos_handler;
$total_term='END';
$objPHPExcel->getActiveSheet()->getStyle($pos_handler)->applyFromArray($class_listex_titl);

$objPHPExcel->getActiveSheet()->mergeCells($merge_this);
$objPHPExcel->getActiveSheet()->setCellValue($total_pos_term, $total_term);



$pos_handler=$pos_handler+3;
$total_pos_term='B'.$pos_handler;
$int_tot=initial_consumable_count($consumable_id);
$merge_this=$total_pos_term.':'.'D'.$pos_handler;
$total_term='STOCK TOTAL: '.$int_tot;
$objPHPExcel->getActiveSheet()->getStyle($pos_handler)->applyFromArray($class_listex_titl);

$objPHPExcel->getActiveSheet()->mergeCells($merge_this);
$objPHPExcel->getActiveSheet()->setCellValue($total_pos_term, $total_term);




$remain_tot=count_remaining_consumable($consumable_id);
$pos_handler=$pos_handler+1;
$total_pos_term='B'.$pos_handler;

$merge_this=$total_pos_term.':'.'D'.$pos_handler;
$total_term='TAKEN: '.($int_tot-$remain_tot);
$objPHPExcel->getActiveSheet()->getStyle($pos_handler)->applyFromArray($class_listex_titl);

$objPHPExcel->getActiveSheet()->mergeCells($merge_this);
$objPHPExcel->getActiveSheet()->setCellValue($total_pos_term, $total_term);


$pos_handler=$pos_handler+1;
$total_pos_term='B'.$pos_handler;
$fig=($remain_tot/$int_tot)*100;
$fig=number_format($fig,2);
$nl=$fig.'%';
$merge_this=$total_pos_term.':'.'D'.$pos_handler;
$total_term='IN-STOCK: '.$remain_tot.' ('.$nl.')';
$objPHPExcel->getActiveSheet()->getStyle($pos_handler)->applyFromArray($class_listex_titl);

$objPHPExcel->getActiveSheet()->mergeCells($merge_this);
$objPHPExcel->getActiveSheet()->setCellValue($total_pos_term, $total_term);





$objPHPExcel->getActiveSheet()->getStyle($total_pos_term)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle($total_pos)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,)
);
$objPHPExcel->getActiveSheet()->getStyle($total_pos_term)->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);
$objPHPExcel->getActiveSheet()->getStyle($total_pos)->getAlignment()->applyFromArray(
    array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,)
);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);



//
//$objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
//$objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
//$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter($exofile_name);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(28);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(28);



/*

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);*/


$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);

$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);

$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

/*$objPHPExcel->getActiveSheet()->setPrintGridlines(FALSE);*/

    $fh='Consumable Report';

$objPHPExcel->getActiveSheet()->setTitle($fh);

$objPHPExcel->setActiveSheetIndex(0);






















$exofile_name=$exofile_name.'__'.date("F jS Y (D) h.iA");
$exofile_name=$exofile_name.'.'.$defa_excel_expo_format;

if($defa_excel_expo_format=='xlsx'){
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

}elseif($defa_excel_expo_format=='xls'){
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=$exofile_name");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 18 Jul 2014 05:00:00 GMT');
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header ('Cache-Control: cache, must-revalidate');
    header ('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}elseif($defa_excel_expo_format=='pdf'){
    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
    $rendererLibrary = 'mPDF5.7';
    $rendererLibraryPath = dirname(__FILE__) . '/ss_vendor/phpExcel/Classes/pdf/';

    if (!PHPExcel_Settings::setPdfRenderer(
        $rendererName,
        $rendererLibraryPath
    )) {
        die(
            'NOTICE: Something ain`t right, kindly contact system developers' .
            '<br />' .
            'ERRORCODE: PDF_LIB404'
        );
    }

    header('Content-Type: application/pdf');
    header("Content-Disposition: attachment;filename=".$exofile_name."");
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

}

$objWriter->save('php://output');
exit;
