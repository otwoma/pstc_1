<?php
/**
 * Created by PhpStorm.
 * User: Otwoma
 * Date: 5/10/2017
 * Time: 9:22 AM
 */

include'_header_store.php';
?>

<div id="main-container">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <h4 class="font-light m-b-xs">
                    <i class="fa fa-plus"></i> New Purchase Request Form </h4>
                <small style="color: #409DDB;">Form to make purchase request</small>
            </div>
        </div>
    </div>

    <div class="content animate-panel">

        <div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            Fill in the Form for a new Purchase Request
                        </div>
                        <div class="panel-body">
                            <form id="main_tf" style="background-color:#F1F3F6; padding:5px; margin:5px;" method="post" enctype="multipart/form-data" action="../_actions.php" class="form-horizontal">
                                <div title="PRISONS STAFF TRAINING COLLEGE - REQUEST FOR PURCHASE FORM" class="form-group">
                                    <div class="col-sm-12 col-sm-offset-2">
                                        <h4>Prisons Staff Training College </h4>

                                        <h4>REQUEST FOR PURCHASE FORM</h4>
                                    </div>
                                </div>

                                <div title="Section Head" class="form-group">
                                    <label class="col-sm-2 control-label">FROM: SECTION HEAD</label>
                                    <div class="col-sm-6">
                                        <div class="input-group m-b"><input required="" name="section_head" type="text" placeholder="Section Head" class="form-control">
                                            <span class="input-group-addon"></span>
                                        </div>
                                    </div>
                                </div>

<!--                                <div title="Procurement Officer" class="form-group"><label class="col-sm-2 control-label">TO: PROCUREMENT OFFICER</label>-->
<!--                                    <div class="col-sm-6">-->
<!--                                        <div class="input-group m-b">-->
<!--                                            <input required="" name="proc_officer" type="text" placeholder="Procurement Officer" class="form-control">-->
<!--                                            <span class="input-group-addon"></span>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->


                                <div title="Purpose" class="form-group">
                                    <label class="col-sm-2 control-label">PURPOSE</label>
                                    <div class="col-sm-6">
                                        <div class="input-group m-b">
                                            <input required="" name="purpose" type="text" placeholder="Purpose" class="form-control">
                                            <span class="input-group-addon"></span>
                                        </div>
                                    </div>
                                </div>

                                <input name="extra" type="hidden" class="hidden" value="7">
                                <input id="item_iter" value="1" class="hidden"/>

                                <table>
                                    <thead>
                                    <tr class="bold">
                                        <td width="42px">S/NO</td>
                                        <td width="320px">Item Description</td>
                                        <td width="100px">Unit of Issue</td>
                                        <td width="100px">Qty</td>
                                        <td width="100px">Unit Cost</td>
                                        <td width="120px">Total Amount</td>
                                        <td width="120px">S13 Stock Balance</td>
                                        <td width="120px">Date Last Purchased</td>
                                        <td>Remarks</td>
                                    </tr>
                                    </thead>

                                    <tr>
                                        <td title="S/No."><input class="form-control" placeholder="Code No." value="1" type="text" name="item_code[]"></td>
                                        <td title="Item Description"><input class="form-control" placeholder="Item Description" type="text" name="item_description[]"></td>
                                        <td title="Unit of Issue"><input class="form-control" placeholder="Unit of Issue" type="text" name="item_issue_unit[]"></td>
                                        <td title="Required Quantity"><input class="form-control" placeholder="Quantity " type="text" name="item_qty_received[]"></td>
                                        <td title="Issued Quantity"><input class="form-control" placeholder="Unit Cost" type="text" name="item_cost[]"></td>
                                        <td title="Value"><input class="form-control" placeholder="Total Amount" type="text" name="item_value[]"></td>
                                        <td title="Value"><input class="form-control" placeholder="S13 Balance" type="text" name="s13_balance[]"></td>
                                        <td title="Value"><input class="form-control" id="date5" placeholder="Last Purchase" type="text" name="last_purchase_date[]"></td>
                                        <td title="Remarks/Purpose"><input class="form-control" placeholder="Remarks" type="text" name="item_remarks[]"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr id="prepender" style="background: #EBEBEB;">
                                        <td></td>
                                        <td>
                                            <button type="button" id="new_wt_item" class="btn btn-info btn-sm col-sm-offset-12">
                                                <i class="fa fa-plus"></i> Add Entry
                                            </button>
                                        </td>

                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>

                                <br>
                                <br>
                                <br>

                                <div class="form-group">
                                    <div class="col-sm-4  col-sm-offset-1">
                                        <label for="req_officer">Requisitioner</label>
                                        <input placeholder="Requisitioner" id="req_officer" name="req_officer" type="text" class="form-control">
                                    </div>

                                    <div class="col-sm-4" title="Date">
                                        <label for="date">Date</label>
                                        <input placeholder="Date" id="date" name="date" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4  col-sm-offset-1">
                                        <label for="account_no">Stores Clerk</label>
                                        <input placeholder="Stores clerk" id="clerk" name="stores_clerk" type="text" class="form-control">
                                    </div>

                                    <div class="col-sm-4" title="Date">
                                        <label for="date">Date</label>
                                        <input placeholder="Date" id="date2" name="date" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4  col-sm-offset-1">
                                        <label for="account_no">Procurement Officer</label>
                                        <input placeholder="Procurement Officer" id="proc_officer" name="proc_officer" type="text" class="form-control">
                                    </div>

                                    <div class="col-sm-4" title="Date">
                                        <label for="date">Date</label>
                                        <input placeholder="Date" id="date3" name="date" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8  col-sm-offset-1">
                                        <label for="received_by">Vote Book Balances</label>
                                        <input placeholder="Vote Book Balances" id="book_balances" name="book_balances" type="text" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4  col-sm-offset-1">
                                        <label for="issued_by">Accountant</label>
                                        <input placeholder="Accountant" id="accountant" name="accountant" type="text" class="form-control">
                                    </div>

                                    <div class="col-sm-4">
                                        <label for="issue_date">Date</label>
                                        <input placeholder="Date" id="date4" name="date" type="text" class="form-control">
                                    </div>
                                </div>

                                <br>

                                <div class="row col-sm-offset-1">
                                    <div class="col-sm-8 pull-left bg-white padding-7" style="border-radius: 5px;">
                                        <div title="Upload Scanned Copy" class="form-group">
                                            <label class="col-sm-4 control-label">Upload Scanned Copy</label>
                                            <div class="col-sm-5 pull-left">
                                                <div class="input-group m-b">
                                                    <input multiple="" name="attach[]" type="file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
</br>

                                <div class="form-group">
                                    <button id="main_xw" class="btn btn-success col-sm-offset-4" type="button"><span
                                            class="fa fa-send"></span> Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


<?php include "../_footer.php"; ?>

<script src="../assets/js/select2.min.js"></script>
<script src="../assets/js/bootstrap-datepicker.min.js"></script>
<script src="../assets/js/bootstrap-timepicker.min.js"></script>

<script>    $(".purchases_mu").addClass('active');

    $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    $.fn.datepicker.defaults.autoclose = "true";
    $('.form-group.date._date').datepicker({});
    $('#date').datepicker({});
    $('#date2').datepicker({});
    $('#date3').datepicker({});
    $('#date4').datepicker({});
    $('#date5').datepicker({});

    $("#new_wt_item").click(function () {
        var counter = $("#item_iter");
        var count = counter.val();
        var new_count = parseInt(count) + 1;
        counter.val(new_count);

        $.post('../_ajax.php', {purchase_data_row: 1, extra: 77, next: new_count},
            function (row_html) {
                $("#prepender").before(row_html);
            });

    });

    $('#main_xw').on('click', function (e) {

        $("#main_tf").submit()
    });

</script>


