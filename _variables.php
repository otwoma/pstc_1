<?php error_reporting(0);
/*TIME ZONE CONS*/
date_default_timezone_set('Africa/Nairobi');

/*MAIN SESSION LABELS*/
session_start();
$tid=$_SESSION['_user_id'];
$act=$_SESSION['_type'];

/*INSTITUTION LABEL CONS*/
$school_name='Prison Staff Training College';


/*SOFTWARE*/
$software_label=$school_name.' System';

/*IMAGE LOCATIONS*/
$avatars_location='../assets/avatars/';
$avad = $avatars_location;

$min_recruit_id_len=3; //submit greater when strlen greater than
$min_instructor_id_len=3;

/*PASSWORD CONS*/
$xcry= new Cry();
$password_key='W%8M3{9?9Mu_8nT^w@K#';

$content_pk = '0Jm/i6&P_|+Q7';

$url_prime_number=$_SESSION['eid'];

include ('assets/arrays/tribes.php');
include ('assets/arrays/counties.php');
include ('assets/arrays/sub_counties.php');
include ('assets/arrays/wards.php');

$registry_permissions = ['Certificate Clearance', 'Medica   l Clearance', 'Fingerprint Clearance','National ID Clearance'];
$registry_permissions_icons = ['fa-certificate','fa-user-md','fa-hand-pointer-o','fa-id-card-o'];

$excel_title = 'KENYA PRISONS TRAINING COLLEGE';


$timetable_refs[] = ['15','16','17','19','110','111','113','114','115'];
$timetable_refs[] = ['21','22','23','25','26','27','29','210','211','213','214','215'];
$timetable_refs[] = ['31','32','33','35','36','37','39','310','311','313','314','315'];
$timetable_refs[] = ['41','42','43','45','46','47','49','410','411','413','414','415'];
$timetable_refs[] = ['51','52','53','59','510','511','513','514','515'];


$timetable_refs_tag[] = ['T15','T16','T17','T19','T110','T111','T113','T114','T115'];
$timetable_refs_tag[] = ['T21','T22','T23','T25','T26','T27','T29','T210','T211','T213','T214','T215'];
$timetable_refs_tag[] = ['T31','T32','T33','T35','T36','T37','T39','T310','T311','T313','T314','T315'];
$timetable_refs_tag[] = ['T41','T42','T43','T45','T46','T47','T49','T410','T411','T413','T414','T415'];
$timetable_refs_tag[] = ['T51','T52','T53','T59','T510','T511','T513','T514','T515'];